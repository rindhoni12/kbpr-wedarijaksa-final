<!DOCTYPE html>
<html>

<head>
    <!-- Head V-->
    @include('component.head')
</head>

<body class="royal_preloader">
    <div id="page" class="site">
        <!-- Header V-->
        @include('component.header')

        <div id="content" class="site-content custom-margin">
            {{-- Gambar diatas --}}
            <div class="page-header">
                <div class="container">
                    <div class="breadc-box no-line">
                        <div class="row">
                            <div class="col-md-6">
                                <h1 class="page-title">Kredit</h1>
                            </div>
                            <div class="col-md-6 mobile-left text-right">
                                <ul id="breadcrumbs" class="breadcrumbs none-style">
                                    <li><a href="{{ route('home') }}">Home</a></li>
                                    <li><a href="{{ route('kredit') }}">Layanan Kami</a></li>
                                    <li class="active">Kredit</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Main Content --}}
            <div class="entry-content">
                <div class="container">
                    <div class="row">

                        <div id="primary" class="content-area col-lg-8 col-md-8 col-sm-12 col-xs-12 pull-right">
                            <main id="main" class="site-main">

                                <article class="ot_service type-ot_service status-publish has-post-thumbnail hentry">
                                    <div class="inner-post">
                                        <section class="wpb_row row-fluid">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="wpb_column column_container col-sm-12">
                                                        <div class="column-inner">
                                                            <div class="wpb_wrapper">
                                                                <h2>Fasilitas Kredit</h2>
                                                                <img src="/images/servis/kredit.jpg"
                                                                    alt="Kredit KBPR Wedarijaksa"
                                                                    style="margin-bottom: 20px">
                                                                <div class="wpb_text_column wpb_content_element">
                                                                    <div class="wpb_wrapper">
                                                                        <p>
                                                                            KBPR Wedarijaksa menyediakan fasilitas
                                                                            kredit kepada nasabah dengan cepat, mudah
                                                                            dan memuaskan untuk keperluan:
                                                                        </p>
                                                                        <ul>
                                                                            <li>
                                                                                Modal kerja
                                                                            </li>
                                                                            <li>
                                                                                Investasi pendukung usaha misalnya
                                                                                untuk
                                                                                membeli mesin
                                                                            </li>
                                                                            <li>
                                                                                Konsumsi misalnya untuk biaya pendidikan
                                                                                anak
                                                                            </li>
                                                                        </ul>
                                                                        <h4>Jangka Waktu Kredit</h4>
                                                                        <p>
                                                                            Produk kredit yang ditawarkan kepada nasabah
                                                                            antara lain :
                                                                        </p>
                                                                        <ul>
                                                                            <li>
                                                                                Pinjaman kredit jangka waktu 10 bulan
                                                                                dengan bunga 2,5 % per bulan
                                                                            </li>
                                                                            <li>
                                                                                Pinjaman kredit jangka waktu 20 bulan
                                                                                dengan bunga 2 % per bulan
                                                                            </li>
                                                                        </ul>
                                                                        <h4>Sistem Bunga Kredit</h4>
                                                                        <p>
                                                                            Perhitungan angsuran pokok dan bunga kredit
                                                                            di KBPR dengan sistem :
                                                                        </p>
                                                                        <ul>
                                                                            <li>
                                                                                Flat / Tetap : Dalam sistem bunga flat
                                                                                (perhitungan bunga
                                                                                didasarkan pada utang awal sehingga
                                                                                angsuran
                                                                                bulanan akan tetap sama selama masa
                                                                                pinjaman
                                                                            </li>
                                                                            <li>
                                                                                Bunga Putus : Sistem bunga putus,
                                                                                artinya bunga yang
                                                                                dikenakan terputus pada saat pelunasan
                                                                            </li>
                                                                        </ul>
                                                                        <h4>Persyaratan Pengajuan</h4>
                                                                        <ol>
                                                                            <li>
                                                                                Fotokopi KTP suami – istri sebanyak 2
                                                                                lembar
                                                                            </li>
                                                                            <li>
                                                                                Fotokopi Kartu Keluarga (KK) sebanyak 1
                                                                                lembar
                                                                            </li>
                                                                            <li>
                                                                                Fotokopi Agunan kredit berupa :
                                                                                <ul>
                                                                                    <li>
                                                                                        BPKB (Fotokopi BPKB dan STNK
                                                                                        sebanyak 1 lembar) atau
                                                                                    </li>
                                                                                    <li>
                                                                                        Sertifikat Tanah (Fotokopi
                                                                                        sertifikat tanah dan tumpi
                                                                                        (surat
                                                                                        pajak). Juga dilampirkan
                                                                                        fotokopi KTP
                                                                                        an.
                                                                                        yang tertera di sertifikat
                                                                                        tersebut)
                                                                                    </li>
                                                                                </ul>
                                                                            </li>
                                                                        </ol>
                                                                    </div>
                                                                </div>
                                                                <div
                                                                    class="row wpb_row inner row-fluid cta-section row-has-fill">
                                                                    <div
                                                                        class="wpb_column column_container col-sm-12 col-md-9">
                                                                        <div class="column-inner">
                                                                            <div class="wpb_wrapper">
                                                                                <div
                                                                                    class="section-head mobile-center text-light">
                                                                                    <h6><span>Tertarik penawaran kredit
                                                                                            kami?</span></h6>
                                                                                    <h2 class="section-title">Coba
                                                                                        simulasi kredit sekarang!</h2>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div
                                                                        class="wpb_column column_container col-sm-12 col-md-3">
                                                                        <div class="column-inner">
                                                                            <div class="wpb_wrapper">
                                                                                <div class="empty_space_12 sm-hidden">
                                                                                </div>
                                                                                <div
                                                                                    class="text-right mobile-center light-hover">
                                                                                    <a class="btn"
                                                                                        href="{{ route('simulasi') }}">COBA
                                                                                        SEKARANG</a>
                                                                                </div>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </article>

                            </main>
                        </div>

                        {{-- Sidebar --}}
                        <aside id="sidebar" class="widget-area service-sidebar col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <section id="nav_menu-1" class="widget widget_nav_menu">
                                <h4 class="widget-title">Layanan Kami</h4>
                                <div class="menu-service-menu-container">
                                    <ul id="menu-service-menu" class="menu">
                                        <li class="current-menu-item">
                                            <a href="{{ route('kredit') }}">Kredit</a>
                                        </li>
                                        <li><a href="{{ route('deposito') }}">Deposito</a></li>
                                        <li><a href="{{ route('tamasya') }}">Tabungan Masyarakat (Tamasya)</a></li>
                                        <li><a href="{{ route('simpel') }}">Simpanan Pelajar (Simpel)</a></li>
                                        <li><a href="{{ route('kurban') }}">Tabungan Kurban (Coming Soon)</a></li>
                                    </ul>
                                </div>
                            </section>
                            @include('component.layanan-sidebar')
                        </aside>

                    </div>
                </div>
            </div>
        </div>

        <!-- footer V-->
        @include('component.footer')
    </div>

    <!-- Script V -->
    @include('component.script')
</body>

</html>