<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('assets/bootstrap/css/bootstrap.min.css') }}">
    <title>Simulasi Kredit</title>
</head>

<style>
    table {
        width: 100%;
    }
</style>

<body>
    <main class="row d-flex justify-content-center">
        <form id="simulasiKredit" method="post" class="col-12 col-md-9 pl-5 pr-5">
            <h1 class="display-3 mb-3 text-center">Simulasi Kredit</h1>

            <div class="form-group">
                <label for="jumlahKredit">Jumlah Kredit <em>(rupiah)</em>: </label>
                <input type="number" class="form-control" id="jumlahKredit" name="jumlahKredit"
                    placeholder="Contoh: 150000000" value="5000000">
            </div>

            <div class="form-group">
                <label for="jangkaWaktu">Jangka Waktu <em>(bulan)</em>: </label>
                <input type="number" class="form-control" id="jangkaWaktu" name="jangkaWaktu" placeholder="Contoh: 10"
                    value="10">
            </div>

            {{-- <div class="form-group">
                <label for="bungaPertahun">Bunga Pertahun <em>(%)</em>: </label>
                <input type="number" class="form-control" id="bungaPertahun" name="bungaPertahun"
                    placeholder="Contoh: 10.5" value="1">
            </div> --}}

            {{-- <div class="form-group" hidden>
                <div class="form-check-inline">
                    <input type="radio" class="form-check-input" value="1" name="metode" id="Flat" checked>
                    <label class="form-check-label" for="Flat">Flat</label>
                </div>
            </div> --}}

            <div class="form-group">
                <button id="btnHitung" type="submit" class="btn btn-success">Hitung</button>
                <button id="btnUlangi" type="submit" class="btn btn-secondary">Ulangi</button>
            </div>

        </form>
    </main>

    <aside>

        <h1 class="display-4 mb-3 text-center">Pinjaman Anda</h1>

        <div class="row d-flex justify-content-center">
            <div class="col-4">Total Pinjaman</div>
            <div class="col-4">: <span id="resultTotalPinjaman"></span></div>
        </div>

        <div class="row d-flex justify-content-center">
            <div class="col-4">Lama Pinjaman</div>
            <div class="col-4">: <span id="resultLamaPinjaman"></span></div>
        </div>

        <div class="row d-flex justify-content-center">
            <div class="col-4">Bunga Pertahun</div>
            <div class="col-4">: <span id="resultBungaPertahun"></span></div>
        </div>

        <div class="flatOnly">

            <div class="row d-flex justify-content-center">
                <div class="col-4">Angsuran Pokok Perbulan</div>
                <div class="col-4">: <span id="resultAngPokokBulan"></span></div>
            </div>

            <div class="row d-flex justify-content-center">
                <div class="col-4">Angsuran Bunga Perbulan</div>
                <div class="col-4">: <span id="resultAngBungaBulan"></span></div>
            </div>

            <div class="row d-flex justify-content-center">
                <div class="col-4">Total angsuran per bulan</div>
                <div class="col-4">: <span id="resultAngBulan"></span></div>
            </div>

        </div>

        <div class="row d-flex justify-content-center mt-3">
            <table id="tableAngsuran" class="col-8">
                <thead class="thead-dark">
                    <tr>
                        <th scope="col">Bulan</th>
                        <th scope="col">Pokok</th>
                        <th scope="col">Bunga</th>
                        <th scope="col">Angsuran</th>
                        <th scope="col">Sisa Pinjaman</th>
                    </tr>
                </thead>
                <tbody id="table_list">

                </tbody>
            </table>
        </div>
    </aside>

</body>
<script src="{{ asset('assets/jquery/jquery-3.5.1.min.js') }}"></script>
<script src="{{ asset('assets/jquery/jquery.validate.min.js') }}"></script>
<script src="{{ asset('assets/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/numeraljs/numeral.min.js') }}"></script>
{{-- <script src="{{ asset('assets/custom.js') }}"></script> --}}

<script>
    $(document).ready(function() {
        $("aside").hide();

        $('#simulasiKredit').on('submit',function(e){
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#table_list').empty();
            let jumlahKredit  = $('#jumlahKredit').val();
            let jangkaWaktu   = $('#jangkaWaktu').val();

            if (jumlahKredit.length == 0) {
                alert("Isi dulu jumlah kredit");
            } else if (jangkaWaktu.length == 0) {
                alert("Pilih salah satu jangka waktu kredit");
            } else {
                $.ajax({
                    url: '{{ route('hitung.kredit') }}',
                    type: 'POST',
                    data: $(this).serialize(),
                    success: function(response){
                        // console.log(response.angsuran);
                        $('#resultTotalPinjaman').text(rupiah_format(response.jumlahKredit));
                        $('#resultLamaPinjaman').text(response.jangkaWaktu + " Bulan");
                        $('#resultBungaPertahun').text(response.sukubungaPertahun + " %");
                        $('#resultAngPokokBulan').text(rupiah_format(response.pokok));
                        $('#resultAngBungaBulan').text(rupiah_format(response.bunga));
                        $('#resultAngBulan').text(rupiah_format(response.jumlahAngsuran));

                        $.each(response.angsuran, function (key, value) {
                            $('#table_list').append(`
                                <tr>
                                    <td>${value.no}</td>
                                    <td>${rupiah_format(value.pokok)}</td>
                                    <td>${rupiah_format(value.bunga)}</td>
                                    <td>${rupiah_format(value.jumlahAngsuran)}</td>
                                    <td>${rupiah_format(value.sisaPinjaman)}</td>
                                </tr>
                            `)
                        });

                        $("aside").show();
                    },
                });
            }            
        });

        $('#btnUlangi').on('click', function(e){
            e.preventDefault();
            $("aside").hide();
            $('#table_list').empty();
        })

        function rupiah_format(number) {
            if (number == 0) {
                return rupiah = 'Rp' + 0;
            } else {
                const numb = number;
                const format = numb.toString().split('').reverse().join('');
                const convert = format.match(/\d{1,3}/g);
                return rupiah = 'Rp' + convert.join('.').split('').reverse().join('') + ',00'
            }
        }
    });
</script>

</html>