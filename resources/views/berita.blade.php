<!DOCTYPE html>
<html>

<head>
    <!-- Head V-->
    @include('component.head')
</head>

<body class="royal_preloader">
    <div id="page" class="site">
        <!-- Header V-->
        @include('component.header')

        <div id="content" class="site-content custom-margin">
            {{-- Gambar diatas --}}
            <div class="page-header">
                <div class="container">
                    <div class="breadc-box no-line">
                        <div class="row">
                            <div class="col-md-6">
                                <h1 class="page-title">Berita Kami</h1>
                            </div>
                            <div class="col-md-6 mobile-left text-right">
                                <ul id="breadcrumbs" class="breadcrumbs none-style">
                                    <li><a href="{{ route('home') }}">Home</a></li>
                                    <li class="active">Berita Kami</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Main Content --}}
            <div class="entry-content">
                <div class="container">
                    <div class="row">

                        {{-- Konten Utama Berita --}}
                        <div id="primary" class="content-area col-lg-8 col-md-8 col-sm-12 col-xs-12">
                            <main id="main" class="site-main">

                                @foreach ($berita as $item)
                                <article class="post-box post type-post hentry">
                                    <div class="entry-media">
                                        <a href="{{ $item['url'] }}">
                                            <img src="{{ $item['image1'] }}" alt="{{ $item['judul'] }}"
                                                style="width: 800px; height: 400px; object-fit:cover; object-position: center;">
                                        </a>
                                    </div>
                                    <div class="inner-post">
                                        <header class="entry-header">

                                            <div class="entry-meta">
                                                <span class="posted-on">
                                                    <time class="entry-date published">{{ $item['tanggal'] }}</time>
                                                </span>
                                            </div>
                                            <!-- .entry-meta -->
                                            <h4 class="entry-title"><a href="{{ $item['url'] }}" rel="bookmark">{{
                                                    $item['judul']
                                                    }}</a></h4>
                                        </header>
                                        <!-- .entry-header -->

                                        <div class="entry-summary">
                                            <p>{{ \Str::limit($item['body'], 150, $end='...') }}</p>
                                        </div>
                                        <!-- .entry-content -->

                                        <footer class="entry-footer">
                                            <a class="post-link" href="{{ $item['url'] }}">Baca selengkapnya</a>
                                        </footer>
                                        <!-- .entry-footer -->
                                    </div>
                                </article>
                                @endforeach

                            </main>
                            <!-- #main -->
                        </div>

                        {{-- Sidebar kanan --}}
                        <aside id="sidebar" class="widget-area primary-sidebar col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <section id="recent_news-1" class="widget widget_recent_news">
                                <h4 class="widget-title">Berita Terbaru</h4>
                                <ul class="recent-news clearfix">

                                    @foreach ($berita_side as $item)
                                    <li class="clearfix ">
                                        <div class="thumb">
                                            <a href="{{ $item['url'] }}">
                                                <img src="{{ $item['image'] }}" alt="{{ $item['judul'] }}"
                                                    style="width: 80px; height: 80px; object-fit:cover; object-position: center;">
                                            </a>
                                        </div>
                                        <div class="entry-header">
                                            <h6>
                                                <a href="{{ $item['url'] }}">{{ $item['judul'] }}</a>
                                            </h6>
                                            <span class="post-on">
                                                <span class="entry-date">{{ $item['tanggal'] }}</span>
                                            </span>
                                        </div>
                                    </li>
                                    @endforeach



                                </ul>

                            </section>
                        </aside>

                    </div>
                </div>
            </div>

        </div>

        <!-- footer V-->
        @include('component.footer')
    </div>

    <!-- Script V -->
    @include('component.script')
</body>

</html>