<!DOCTYPE html>
<html>

<head>
    <!-- Head V-->
    @include('component.head')
</head>

<body class="royal_preloader">
    <div id="page" class="site">
        <!-- Header V-->
        @include('component.header')

        <div id="content" class="site-content custom-margin">
            {{-- Gambar diatas --}}
            <div class="page-header">
                <div class="container">
                    <div class="breadc-box no-line">
                        <div class="row">
                            <div class="col-md-6">
                                <h1 class="page-title">Kontak Kami</h1>
                            </div>
                            <div class="col-md-6 mobile-left text-right">
                                <ul id="breadcrumbs" class="breadcrumbs none-style">
                                    <li><a href="{{ route('home') }}">Home</a></li>
                                    <li class="active">Kontak Kami</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Main Content --}}
            <div class="entry-content">
                <div class="container">
                    <div class="boxed-content">

                        <section class="wpb_row row-fluid section-padd no-bot">
                            <div class="container">
                                <div class="row">
                                    <div class="wpb_column column_container col-sm-12 col-md-12">
                                        <div class="column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="section-head ">
                                                    <h6><span>Lokasi Kantor</span></h6>
                                                    <h2 class="section-title">
                                                        Jl. Raya Juana-Tayu Km.7, Guyangan Kec. Trangkil Kab. Pati
                                                    </h2>
                                                </div>

                                                <div class="empty_space_12"><span class="empty_space_inner"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column column_container col-sm-12 col-md-8">
                                        <div class="column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element contact-info">
                                                    <div class="wpb_wrapper">
                                                        <p>
                                                            <a href="mailto:kbpr.pati@gmail.com">
                                                                <i class="fa fa-envelope">
                                                                    kbpr.pati@gmail.com
                                                                </i>
                                                            </a>
                                                            <a href="tel:4199073">
                                                                <i class="fa fa-phone-square">
                                                                    (0295) 4199073
                                                                </i>
                                                            </a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column column_container col-sm-12 col-md-4">
                                        <div class="column-inner">
                                            <div class="wpb_wrapper">
                                                <div
                                                    class="wpb_text_column wpb_content_element socials text-right md-hidden sm-hidden">
                                                    <div class="wpb_wrapper">
                                                        <p>
                                                            <a href="#" target="_blank" rel="noopener noreferrer">
                                                                <i class="fa fa-facebook-official">fb</i>
                                                            </a>
                                                            <a href="#" target="_blank" rel="noopener noreferrer">
                                                                <i class="fa fa-twitter">tw</i>
                                                            </a>
                                                            <a href="#" target="_blank" rel="noopener noreferrer">
                                                                <i class="fa fa-pinterest">pr</i>
                                                            </a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section style="margin-top: 30px">
                            <div class="container">
                                <div class="row">
                                    <iframe
                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3962.8803638379372!2d111.09416161436485!3d-6.66174566697648!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e772ad5771a3afb%3A0xe749d504c33e504!2sKoperasi%20Bank%20Perkreditan%20Rakyat!5e0!3m2!1sid!2sid!4v1641217024980!5m2!1sid!2sid"
                                        width="1170" height="500" style="border:0;" allowfullscreen="" loading="lazy">
                                    </iframe>
                                </div>
                            </div>
                        </section>
                        <section class="wpb_row row-fluid section-padd no-bot">
                            <div class="container">
                                <div class="row">
                                    <div class="wpb_column column_container col-sm-12">
                                        <div class="column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="gray-line">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        {{-- <section class="wpb_row row-fluid address-section">
                            <div class="container">
                                <div class="row">
                                    <div class="wpb_column column_container col-sm-6">
                                        <div class="column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper">
                                                        <h4>London Office</h4>
                                                        <p>007 OceanThemes St, Broughton Rd, London, England</p>
                                                        <div class="empty_space_30"><span
                                                                class="empty_space_inner"></span></div>
                                                    </div>
                                                </div>

                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper">
                                                        <h4>Berlin Office</h4>
                                                        <p>Hans-Wilhelm-Gasse 0/3, 98051 Schwarzenberg</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column column_container col-sm-6">
                                        <div class="column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper">
                                                        <h4>New York Office</h4>
                                                        <p>6803 Dickens Islands Apt. 567, Port Malikaview, TX 14942</p>
                                                        <div class="empty_space_30"><span
                                                                class="empty_space_inner"></span></div>
                                                    </div>
                                                </div>

                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper">
                                                        <h4>Madrid Office</h4>
                                                        <p>Travessera Eric, 896, 59° 8°, 61008, Sarabia Alta</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section> --}}

                    </div>
                </div>
            </div>
        </div>

        <!-- footer V-->
        @include('component.footer')
    </div>

    <!-- Script V -->
    @include('component.script')
</body>

</html>