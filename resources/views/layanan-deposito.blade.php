<!DOCTYPE html>
<html>

<head>
    <!-- Head V-->
    @include('component.head')
</head>

<body class="royal_preloader">
    <div id="page" class="site">
        <!-- Header V-->
        @include('component.header')

        <div id="content" class="site-content custom-margin">
            {{-- Gambar diatas --}}
            <div class="page-header">
                <div class="container">
                    <div class="breadc-box no-line">
                        <div class="row">
                            <div class="col-md-6">
                                <h1 class="page-title">Deposito Berjangka</h1>
                            </div>
                            <div class="col-md-6 mobile-left text-right">
                                <ul id="breadcrumbs" class="breadcrumbs none-style">
                                    <li><a href="{{ route('home') }}">Home</a></li>
                                    <li><a href="{{ route('kredit') }}">Layanan Kami</a></li>
                                    <li class="active">Deposito Berjangka</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Main Content --}}
            <div class="entry-content">
                <div class="container">
                    <div class="row">

                        <div id="primary" class="content-area col-lg-8 col-md-8 col-sm-12 col-xs-12 pull-right">
                            <main id="main" class="site-main">

                                <article class="ot_service type-ot_service status-publish has-post-thumbnail hentry">
                                    <div class="inner-post">
                                        <section class="wpb_row row-fluid">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="wpb_column column_container col-sm-12">
                                                        <div class="column-inner">
                                                            <div class="wpb_wrapper">
                                                                <h2>Deposito Berjangka</h2>
                                                                <img src="/images/servis/deposito.jpg"
                                                                    alt="Deposito KBPR Wedarijaksa"
                                                                    style="margin-bottom: 20px">
                                                                <div class="wpb_text_column wpb_content_element">
                                                                    <div class="wpb_wrapper">
                                                                        <p>
                                                                            <strong>Deposito Berjangka </strong> adalah
                                                                            simpanan
                                                                            masyarakat yang ditempatkan
                                                                            dibank, dimana penarikannya hanya dapat
                                                                            dilakukan pada waktu bertentu, berdasarkan
                                                                            perjanjian nasabah penyimpan dengan bank
                                                                            (time deposit)
                                                                        </p>
                                                                        <h4>Syarat Pembukaan Rekening</h4>
                                                                        <ol>
                                                                            <li>
                                                                                Fotocopy KTP yang masih
                                                                                berlaku
                                                                            </li>
                                                                            <li>
                                                                                Fotocopy KTP ahli waris yang
                                                                                ditunjuk
                                                                            </li>
                                                                            <li>
                                                                                Mengisi formulir permohonan
                                                                                pembukaan rekening deposito
                                                                                berjangka
                                                                                dengan lengkap dan jelas
                                                                            </li>
                                                                        </ol>
                                                                        <h4>Keuntungan Deposito Berjangka</h4>
                                                                        <ul>
                                                                            <li>Bunga umumnya lebih menarik</li>
                                                                            <li>
                                                                                Memiliki jangka waktu tertentu sesuai
                                                                                dengan kesepakatan
                                                                            </li>
                                                                            <li>
                                                                                Deposito KBPR juga dijamin oleh LPS
                                                                                sesuai
                                                                                ketentuan dan persyaratan yang berlaku
                                                                            </li>
                                                                            <li>Dapat dijadikan agunan kredit</li>
                                                                        </ul>
                                                                        <h4>Bunga dan Jangka Waktu Deposito</h4>
                                                                        <ul>
                                                                            <li>
                                                                                Deposito Berjangka dengan tenor 1 bulan
                                                                                tingkat suku bunga 5% per tahun atau
                                                                                0,42%
                                                                                per bulan
                                                                            </li>
                                                                            <li>
                                                                                Deposito Berjangka dengan tenor 3 bulan
                                                                                tingkat suku bunga 6% per tahun atau
                                                                                0,5%
                                                                                per bulan
                                                                            </li>
                                                                            <li>
                                                                                Deposito Berjangka dengan tenor 6 bulan
                                                                                tingkat suku bunga mengikuti suku
                                                                                bunga
                                                                                yang berlaku sesuai LPS
                                                                            </li>
                                                                            <li>
                                                                                Deposito Berjangka dengan tenor 6 bulan
                                                                                tingkat suku bunga 9,6% per tahun bagi
                                                                                deposan (Tidak dijamin LPS)
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </article>
                            </main>
                        </div>

                        {{-- Sidebar --}}
                        <aside id="sidebar" class="widget-area service-sidebar col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <section id="nav_menu-1" class="widget widget_nav_menu">
                                <h4 class="widget-title">Layanan Kami</h4>
                                <div class="menu-service-menu-container">
                                    <ul id="menu-service-menu" class="menu">
                                        <li><a href="{{ route('kredit') }}">Kredit</a></li>
                                        <li class="current-menu-item">
                                            <a href="{{ route('deposito') }}">Deposito</a>
                                        </li>
                                        <li><a href="{{ route('tamasya') }}">Tabungan Masyarakat (Tamasya)</a></li>
                                        <li><a href="{{ route('simpel') }}">Simpanan Pelajar (Simpel)</a></li>
                                        <li><a href="{{ route('kurban') }}">Tabungan Kurban (Coming Soon)</a></li>
                                    </ul>
                                </div>
                            </section>
                            @include('component.layanan-sidebar')
                        </aside>

                    </div>
                </div>
            </div>
        </div>

        <!-- footer V-->
        @include('component.footer')
    </div>

    <!-- Script V -->
    @include('component.script')
</body>

</html>