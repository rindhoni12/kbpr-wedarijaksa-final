{{-- <script src="{{ asset('assets/js/scripts.js') }}"></script> --}}

<!-- Additional Script -->
<script type="text/javascript" src="{{ asset('revolution/js/jquery.themepunch.tools.min.js?rev=5.0') }}">
</script>
<script type="text/javascript" src="{{ asset('revolution/js/jquery.themepunch.revolution.min.js?rev=5.0') }}">
</script>
<!-- RS5.0 Extensions Files -->
<script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.video.min.js') }}">
</script>
<script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.slideanims.min.js') }}">
</script>
<script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.layeranimation.min.js') }}">
</script>
<script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.navigation.min.js') }}">
</script>
<script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.actions.min.js') }}">
</script>
<script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.kenburn.min.js') }}">
</script>
<script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.migration.min.js') }}">
</script>
<script type="text/javascript" src="{{ asset('revolution/js/extensions/revolution.extension.parallax.min.js') }}">
</script>
<script>
    jQuery(document).ready(function () {
                jQuery('#revolution-slider').revolution({
                    sliderType: 'standard',
                    delay: 7500,
                    navigation: {
                        arrows: { enable: true },
                    },
                    spinner: 'off',
                    gridwidth: 1170,
                    gridheight: 700,
                    disableProgressBar: 'on',
                    responsiveLevels: [1920, 1229, 991, 480],
                    gridwidth: [1170, 970, 750, 450],
                    gridheight: [700, 700, 700, 700],
                });
            });
</script>