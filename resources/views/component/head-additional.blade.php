<!-- Additional Head -->
<!-- RS5.0 Main Stylesheet -->
<link rel="stylesheet" type="text/css" href="{{ asset('revolution/css/settings.css') }}" />

<!-- RS5.0 Layers and Navigation Styles -->
<link rel="stylesheet" type="text/css" href="{{ asset('revolution/css/layers.css') }}" />
<link rel="stylesheet" type="text/css" href="{{ asset('revolution/css/navigation.css') }}" />