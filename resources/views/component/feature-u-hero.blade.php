<section class="wpb_row row-fluid top-70 row-has-fill relative bg-light">
    <div class="container">
        <div class="row">
            <div class="wpb_column column_container col-sm-12">
                <div class="column-inner">
                    <div class="wpb_wrapper">
                        <div class="empty_space_70 lg-hidden">
                            <span class="empty_space_inner"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column column_container col-sm-6 col-md-3">
                <div class="column-inner">
                    <div class="wpb_wrapper">
                        <div class="service-box icon-box box-shadow-2 ionic">
                            <i class="ion-md-card ion-ios-card ion-logo-card ion-ios-card"></i>
                            <div class="content-box">
                                <h5>Kredit</h5>
                                <p>
                                    Kami menyediakan fasilitas kredit untuk : Modal kerja, Investasi
                                    pendukung usaha, Konsumsi.
                                </p>
                                <a class="link-box pagelink" href="{{ route('kredit') }}">Baca selengkapnya
                                </a>
                            </div>
                        </div>

                        <div class="empty_space_30 lg-hidden">
                            <span class="empty_space_inner"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column column_container col-sm-6 col-md-3">
                <div class="column-inner">
                    <div class="wpb_wrapper">
                        <div class="service-box icon-box box-shadow-2 ionic">
                            <i class="ion-md-albums ion-ios-albums ion-logo-albums ion-ios-albums"></i>
                            <div class="content-box">
                                <h5>Deposito Berjangka</h5>
                                <p>
                                    Kami menyediakan Deposito berjangka waktu bertentu sesuai perjanjian
                                    nasabah dengan Bank.
                                </p>
                                <a class="link-box pagelink" href="{{ route('deposito') }}">Baca selengkapnya
                                </a>
                            </div>
                        </div>

                        <div class="empty_space_30 lg-hidden">
                            <span class="empty_space_inner"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column column_container col-sm-6 col-md-3">
                <div class="column-inner">
                    <div class="wpb_wrapper">
                        <div class="service-box icon-box box-shadow-2 ionic">
                            <i class="ion-md-cash ion-ios-cash ion-logo-cash ion-ios-cash"></i>
                            <div class="content-box">
                                <h5>Tabungan Masyarakat (Tamasya)</h5>
                                <p>
                                    Kami menyediakan Tabungan Masyarakat yang ditujukan untuk masyarakat
                                    umum.
                                </p>
                                <a class="link-box pagelink" href="{{ route('tamasya') }}">Baca selengkapnya
                                </a>
                            </div>
                        </div>

                        <div class="empty_space_30 lg-hidden">
                            <span class="empty_space_inner"></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column column_container col-sm-6 col-md-3">
                <div class="column-inner">
                    <div class="wpb_wrapper">
                        <div class="service-box icon-box box-shadow-2 ionic">
                            <i class="ion-md-people ion-ios-people ion-logo-people ion-ios-people"></i>
                            <div class="content-box">
                                <h5>Simpanan Pelajar (Simpel)</h5>
                                <p>
                                    Kami menyediakan Tabungan untuk para siswa dengan berbagai fitur
                                    menarik.
                                </p>
                                <a class="link-box pagelink" href="{{ route('simpel') }}">Baca selengkapnya
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>