<header id="site-header" class="site-header header-white header-fixed sticky-header mobile-header-blue">
    <!-- Main header start - Header Home-1, Default -->
    <!-- Top bar start / class css: topbar-dark -->
    <div id="header_topbar" class="header-topbar md-hidden sm-hidden clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <!-- contact info -->
                    <ul class="info-list fleft">
                        <li>
                            <i class="fa fa-map-marker"></i> Jl. Raya Juana-Tayu Km.7, Guyangan Kec.
                            Trangkil Kab. Pati
                        </li>
                        <li><i class="fa fa-clock-o"></i>Senin - Jum'at pukul 08.00 - 16.00</li>
                        <li><i class="fa fa-phone"></i><a href="tel:4199073">(0295) 4199073</a></li>
                    </ul>
                    <!-- contact info close -->

                    <!-- social icons -->
                    <ul class="social-list fright social_on_right_side">
                        <li>
                            <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li>
                            <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li>
                            <a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                        </li>
                        <li>
                            <a href="#" target="_blank"><i class="fa fa-rss"></i></a>
                        </li>
                    </ul>
                    <!-- social icons close -->
                </div>
            </div>
        </div>
    </div>
    <!-- Top bar close -->

    <!-- Main header start -->
    <div class="main-header md-hidden sm-hidden">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="col-wrap-table">
                        <div id="site-logo" class="site-logo col-media-left col-media-middle">
                            <a href="{{ route('home') }}">
                                <img src="/images/logo-kbpr3.png" alt="KBPR Wedarijaksa" />
                            </a>
                        </div>
                        <div class="header-mainnav col-media-body col-media-middle">
                            <div id="site-navigation" class="main-navigation fright">
                                <ul id="primary-menu" class="menu">
                                    <li><a href="{{ route('home') }}">Home</a></li>
                                    <li><a href="{{ route('kredit') }}">Kredit</a></li>
                                    <li><a href="{{ route('deposito') }}">Deposito</a></li>
                                    <li class="
                                            menu-item-type-custom
                                            menu-item-object-custom
                                            menu-item-has-children
                                        ">
                                        <a href="#">Tabungan</a>
                                        <ul class="sub-menu">
                                            <li class="menu-item-1738">
                                                <a href="{{ route('tamasya') }}">Tabungan Masyarakat (Tamasya)</a>
                                            </li>
                                            <li class="menu-item-1745">
                                                <a href="{{ route('simpel') }}">Simpanan Pelajar (Simpel)</a>
                                            </li>
                                            <li class="menu-item-1745">
                                                <a href="{{ route('kurban') }}">Tabungan Kurban (Coming Soon)</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="
                                            menu-item-type-custom
                                            menu-item-object-custom
                                            menu-item-has-children
                                        ">
                                        <a href="#">Simulasi Kredit</a>
                                        <ul class="sub-menu">
                                            <li class="menu-item-1738">
                                                <a href="{{ route('simulasi') }}">Simulasi Kredit Standar</a>
                                            </li>
                                            <li class="menu-item-1745">
                                                <a href="{{ route('simulasi.putus') }}">Simulasi Kredit Bunga Putus</a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li class="
                                            menu-item-type-custom
                                            menu-item-object-custom
                                            menu-item-has-children
                                        ">
                                        <a href="#">Laporan</a>
                                        <ul class="sub-menu">
                                            <li class="menu-item-1738">
                                                {{-- <a href="{{ route('tahunan') }}">Laporan Tahunan</a> --}}
                                                <a href="{{ route('triwulan') }}">Laporan Triwulan</a>
                                                <a href="{{ route('gcg') }}">Laporan GCG</a>
                                            </li>
                                        </ul>
                                    </li>
                                    {{-- <li><a href="{{ route('simulasi') }}">Simulasi Kredit</a></li> --}}
                                    <li><a href="{{ route('tentang') }}">Tentang Kami</a></li>
                                    <li>
                                        <a href="https://api.whatsapp.com/send?phone=6281392006441" target="_blank"
                                            style="color: green">
                                            <i class="fa fa-whatsapp" style="margin-right: 8px"></i>
                                            WhatsApp
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <!-- #site-navigation -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Main header close -->
    <div class="header_mobile">
        <div class="mlogo_wrapper clearfix">
            <div class="mobile_logo">
                <a href="{{ route('home') }}"><img src="/images/logo-kbpr3.png" alt="KBPR Wedarijaksa" /></a>
            </div>
            <div id="mmenu_toggle">
                <button></button>
            </div>
        </div>
        <div class="mmenu_wrapper">
            <div class="mobile_nav collapse">
                <ul id="menu-main-menu" class="mobile_mainmenu">
                    <li><a href="{{ route('home') }}">Home</a></li>
                    <li><a href="{{ route('kredit') }}">Kredit</a></li>
                    <li><a href="{{ route('deposito') }}">Deposito</a></li>
                    <li class="
                            menu-item-type-custom
                            menu-item-object-custom
                            menu-item-has-children
                            menu-item-1731
                        ">
                        <a href="#">Tabungan</a>
                        <ul class="sub-menu">
                            <li class="menu-item-1738">
                                <a href="{{ route('tamasya') }}">Tabungan Masyarakat (Tamasya)</a>
                            </li>
                            <li class="menu-item-1745">
                                <a href="{{ route('simpel') }}">Simpanan Pelajar (Simpel)</a>
                            </li>
                            <li class="menu-item-1745">
                                <a href="{{ route('kurban') }}">Tabungan Kurban (Coming Soon)</a>
                            </li>
                        </ul>
                    </li>
                    <li class="
                            menu-item-type-custom
                            menu-item-object-custom
                            menu-item-has-children
                            menu-item-1731
                        ">
                        <a href="#">Simulasi Kredit</a>
                        <ul class="sub-menu">
                            <li class="menu-item-1738">
                                <a href="{{ route('simulasi') }}">Simulasi Kredit Standar</a>
                            </li>
                            <li class="menu-item-1745">
                                <a href="{{ route('simulasi.putus') }}">Simulasi Kredit Bunga Putus</a>
                            </li>
                        </ul>
                    </li>
                    <li class="
                            menu-item-type-custom
                            menu-item-object-custom
                            menu-item-has-children
                        ">
                        <a href="#">Laporan</a>
                        <ul class="sub-menu">
                            <li class="menu-item-1738">
                                {{-- <a href="{{ route('tahunan') }}">Laporan Tahunan</a> --}}
                                <a href="{{ route('triwulan') }}">Laporan Triwulan</a>
                                <a href="{{ route('gcg') }}">Laporan GCG</a>
                            </li>
                        </ul>
                    </li>
                    {{-- <li><a href="{{ route('simulasi') }}">Simulasi Kredit</a></li> --}}
                    <li><a href="{{ route('tentang') }}">Tentang Kami</a></li>
                    <li>
                        <a href="https://api.whatsapp.com/send?phone=6281392006441" target="_blank">
                            <i class="fa fa-whatsapp" style="margin-right: 8px"></i>
                            WhatsApp
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</header>