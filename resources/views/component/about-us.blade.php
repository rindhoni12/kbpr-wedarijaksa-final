<section class="
                        wpb_row
                        row-fluid
                        section-padd
                        row-has-fill row-o-equal-height row-o-content-middle row-flex
                        bg-light
                    ">
    <div class="container">
        <div class="row">
            <div class="wpb_column column_container col-sm-12">
                <div class="column-inner">
                    <div class="wpb_wrapper">
                        <div class="
                                                row
                                                wpb_row
                                                inner
                                                row-fluid row-o-equal-height row-o-content-middle row-flex
                                            ">
                            <div class="
                                                    wpb_column
                                                    column_container
                                                    col-sm-12 col-md-6 col-has-fill
                                                    custom-padd-1
                                                ">
                                <div class="column-inner">
                                    <div class="wpb_wrapper"></div>
                                </div>
                            </div>
                            <div class="bg-second wpb_column column_container col-sm-12 col-md-6">
                                <div class="column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="empty_space_60 lg-hidden h60">
                                            <span class="empty_space_inner"></span>
                                        </div>

                                        <div class="section-head padding-box-2 text-light" style="margin-top: 50px">
                                            <h6><span>TENTANG KAMI</span></h6>
                                            <h2 class="section-title">SEJARAH</h2>
                                        </div>

                                        <div class="
                                                                wpb_text_column wpb_content_element
                                                                padding-box-2
                                                                text-light
                                                            ">
                                            <div class="wpb_wrapper">
                                                <p>
                                                    Koperasi BPR Wedarijaksa Kabupaten Pati (KBPR Wedarijaksa) berlokasi
                                                    di Jalan Raya Juana-Tayu Km.7, Guyangan Kecamatan Trangkil Kabupaten
                                                    Pati. Didirikan berdasarkan Surat Pengurus Koperasi BPR Wedarijaksa
                                                    No: 21/KBPR/IV/1990 tanggal 2 April 1990 dan No : 24/KBPR/VI/1990
                                                    tanggal 18 Juni 1990 perihal permohonan izin usaha Koperasi BPR
                                                    Wedarijaksa di Kecamatan Wedarijaksa, Kabupaten Pati, Jawa Tengah
                                                    dan dengan surat Menteri Keuangan No. S-944/MK.13/1989 tanggal 9
                                                    Agustus 1989.
                                                </p>
                                                <h2 class="section-title">VISI</h2>
                                                <p>
                                                    “Menjadi BPR yang kuat, dipercaya dan selalu dihati masyarakat di
                                                    seluruh wilayah kerja Kabupaten Pati dan sekitarnya.”
                                                </p>
                                                <h2 class="section-title">MISI</h2>
                                                <p>
                                                    “Menjadi BPR yang kuat, dipercaya dan selalu dihati masyarakat
                                                    dengan berkomitmen untuk pengembangan UMKM dengan pelayanan terbaik,
                                                    dapat dipercaya, dan menjadi tumpuan masyarakat disekitar Wilayah
                                                    Kabupaten Pati dan sekitarnya.”
                                                </p>
                                            </div>
                                        </div>
                                        <div class="
                                                                wpb_text_column wpb_content_element
                                                                paddtop-75
                                                                padding-box-2
                                                                info-box
                                                                text-light
                                                            ">
                                            {{-- <div class="wpb_wrapper">
                                                <div class="sign">
                                                    <p>
                                                        <img class="alignnone size-full wp-image-1087"
                                                            src="images/sign1.png" alt="" width="79" height="49" />
                                                    </p>
                                                    <h5>MOH. MAHFUD</h5>
                                                    <p>Direktur Utama</p>
                                                </div>
                                            </div> --}}
                                        </div>
                                        <div class="empty_space_60 lg-hidden">
                                            <span class="empty_space_inner"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- diawasi -->
            <div class="wpb_column column_container col-sm-12">
                <div class="column-inner">
                    <div class="wpb_wrapper">
                        <div class="section-head has-line">
                            <div class="empty_space_45"></div>
                            <h6><span>terdaftar & diawasi oleh</span></h6>
                        </div>
                        <div class="empty_space_60"></div>
                        <div class="partner-slider image-carousel text-center" data-show="4" data-arrow="false"
                            style="align-items: center">

                            <div>
                                <div class="partner-item text-center clearfix">
                                    <div class="inner">
                                        <div class="thumb">
                                            <img src="/images/diawasi/ojk.png" alt="OJK"
                                                style="margin-top: 15px; width:125px" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <div class="partner-item text-center clearfix">
                                    <div class="inner">
                                        <div class="thumb">
                                            <img src="/images/diawasi/bpr.png" alt="BPR" style="width:50px" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <div class="partner-item text-center clearfix">
                                    <div class="inner">
                                        <div class="thumb">
                                            <img src="/images/diawasi/ayobank.png" alt="AYOKEBANK"
                                                style="margin-top: 15px; width:155px" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div>
                                <div class="partner-item text-center clearfix">
                                    <div class="inner">
                                        <div class="thumb">
                                            <img src="/images/diawasi/lps.png" alt="LPS"
                                                style="margin-top: 15px; width:125px" />
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>