<section id="text-1" class="widget widget_text bg-second text-light">
    <h4 class="widget-title">Tertarik dengan Layanan Kami?</h4>
    <div class="textwidget">
        <ul class="semi-bold">
            <li>
                <p>Anda dapat menghubungi kami di :</p>
            </li>
            <li><span class="normal">WhatsApp :</span>
                <a href="https://api.whatsapp.com/send?phone=6281392006441">+6281392006441</a>
            </li>
            <li><span class="normal">E-mail :</span> <a href="mailto:kbpr.pati@gmail.com">kbpr.pati@gmail.com</a>
            </li>
            <li><span class="normal">Kantor :</span> Jl. Raya Juana-Tayu Km.7, Guyangan Kec. Trangkil,
                <br> Kab. Pati
            </li>
        </ul>
        <div class="gaps style-parent"></div>
    </div>
</section>