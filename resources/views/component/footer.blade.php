<footer id="site-footer" class="site-footer bg-second">
    <div class="main-footer">
        <div class="container">
            <div class="row">
                {{-- About Us --}}
                <div class="col-md-3 col-sm-6">
                    <h4 class="widget-title">Informasi</h4>
                    <div id="media_image-1" class="widget widget_media_image">
                        <a href="{{ route('home') }}"><img src="/images/logo-footer.png"
                                alt="LOGO KBPR Wedarijaksa" /></a>
                    </div>
                    <div id="custom_html-1" class="widget_text widget widget_custom_html">
                        <div class="textwidget custom-html-widget">
                            <p>
                                <strong>Jalan Raya Juana-Tayu Km.7, Guyangan Kec. Trangkil, <br />Kab. Pati</strong>
                                <br />
                                <br />
                                <strong>Telp</strong> : (0295) 4199073
                                <br />
                                <strong>WhatsApp</strong> : 0813-9200-6441
                                <br />
                                <strong>Email</strong>: kbpr.pati@gmail.com
                            </p>
                        </div>
                    </div>
                </div>

                {{-- Layanan Kami --}}
                <div class="col-md-3 col-sm-6">
                    <section id="custom_html-2" class="widget_text widget widget_custom_html padding-left">
                        <h4 class="widget-title">Layanan Kami</h4>
                        <div class="textwidget custom-html-widget">
                            <ul class="padd-left">
                                <li><a href="{{ route('kredit') }}">Kredit</a></li>
                                <li><a href="{{ route('deposito') }}">Deposito</a></li>
                                <li><a href="{{ route('tamasya') }}">Tabungan Masyarakat</a></li>
                                <li><a href="{{ route('simpel') }}">Simpanan Pelajar</a></li>
                                <li><a href="{{ route('kurban') }}">Tabungan Kurban</a></li>
                                <li><a href="{{ route('tentang') }}">Tentang Kami</a></li>
                                <li><a href="{{ route('simulasi') }}">Simulasi Kredit Standar</a></li>
                                <li><a href="{{ route('simulasi.putus') }}">Simulasi Kredit Bunga Putus</a></li>
                            </ul>
                        </div>
                    </section>
                </div>

                {{-- Diawasi oleh --}}
                <div class="col-md-3 col-sm-6">
                    <section id="custom_html-3" class="widget_text widget widget_custom_html padding-left">
                        <h4 class="widget-title">Diawasi oleh</h4>
                        <div id="media_image-1" class="widget widget_media_image">
                            <a href="https://www.ojk.go.id/Default.aspx">
                                <img src="/images/diawasi/ojk-bg.jpg" alt="OJK" style="width:145px" />
                            </a>
                        </div>
                        <div id="media_image-1" class="widget widget_media_image">
                            <a href="https://www.lps.go.id/">
                                <img src="/images/diawasi/lps-bg.jpg" alt="LPS" style="width:145px" />
                            </a>
                        </div>
                        <div id="media_image-1" class="widget widget_media_image">
                            <a href="http://ayokebank.com/">
                                <img src="/images/diawasi/ayobank-bg.jpg" alt="AYOKEBANK" style="width:145px" />
                            </a>
                        </div>
                    </section>
                </div>

                {{-- Hubungi Kami --}}
                <div class="col-md-3 col-sm-6">
                    <div id="custom_html-1" class="widget_text widget widget_custom_html">
                        <div class="textwidget custom-html-widget">
                            <h4>Sosial Media Kami</h4>
                            <div class="ot-socials bg-white">
                                <a target="_blank" href="https://api.whatsapp.com/send?phone=6281392006441"
                                    rel="noopener noreferrer"><i class="fa fa-whatsapp"></i></a>
                                <a target="_blank" href="#" rel="noopener noreferrer"><i class="fa fa-facebook"></i></a>
                                <a target="_blank" href="#" rel="noopener noreferrer"><i
                                        class="fa fa-instagram"></i></a>
                                <a target="_blank" href="#" rel="noopener noreferrer"><i class="fa fa-twitter"></i></a>
                                <a target="_blank" href="#" rel="noopener noreferrer"><i class="fa fa-youtube"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end col-lg-3 -->
            </div>
        </div>
    </div>
    <!-- .main-footer -->
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-copyright text-center">
                        © 2021 Koperasi BPR Wedarijaksa
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- .copyright-footer -->
    <a id="back-to-top" href="#" class="show"></a>
</footer>