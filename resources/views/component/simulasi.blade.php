{{-- <section class="wpb_row row-fluid section-padd bg-second row-has-fill">
    <div class="container">
        <div class="row">
            <div class="wpb_column column_container col-sm-12">
                <div class="column-inner">
                    <div class="wpb_wrapper">
                        <h2 class="custom_heading text-light">Simulasi Kredit</h2>
                        <div class="wpb_text_column wpb_content_element text-light">
                            <div class="wpb_wrapper">
                                <p>
                                    Simulasi ini untuk memudahkan calon kreditur mengetahui besaran
                                    angsuran per-bulan yang harus dibayarkan dan besarannya sudah sesuai
                                    aturan bunga yang ditetapkan perusahaan per tanggal 01 Januari 2021.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column column_container col-sm-12">
                <div class="column-inner">
                    <div class="wpb_wrapper">
                        <div role="form" class="wpcf7" id="wpcf7-f1626-p1530-o1" lang="en-US" dir="ltr">
                            <div class="screen-reader-response"></div>
                            <form id="simulasiKredit" method="post" class="wpcf7-form">
                                <div class="row">
                                    <div class="col-md-6">
                                        <span class="wpcf7-form-control-wrap your-name">
                                            <h5 class="text-light">Jumlah Pinjaman</h5>
                                            <input type="number" id="jumlahKredit" name="jumlahKredit" size="40" class="
                                                    wpcf7-form-control
                                                    wpcf7-text
                                                    wpcf7-validates-as-required
                                                " required="" aria-invalid="false" placeholder="Contoh : 50.000.000" />
                                        </span>
                                    </div>
                                    <div class="col-md-6">
                                        <span class="wpcf7-form-control-wrap your-service">
                                            <h5 class="text-light">Lama Pinjaman</h5>
                                            <select id="jangkaWaktu" name="jangkaWaktu"
                                                class="wpcf7-form-control wpcf7-select" aria-invalid="false">
                                                <option value="10">
                                                    10 Bulan (2,5% per Bulan)
                                                </option>
                                                <option value="20">
                                                    20 Bulan (2% per Bulan)
                                                </option>
                                            </select>
                                        </span>
                                    </div>
                                </div>
                                <p>
                                    <button id="btnHitung" type="submit"
                                        class="wpcf7-form-control wpcf7-submit btn">Hitung</button>
                                    <button id="btnUlangi" type="submit"
                                        class="wpcf7-form-control wpcf7-submit btn">Ulangi</button>
                                </p>
                                <div class="wpcf7-response-output wpcf7-display-none"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}
<section class="wpb_row row-fluid bg-light section-padd no-top">
    <div class="container">
        <div class="row">
            <div class="wpb_column column_container col-sm-12">
                <div class="column-inner">
                    <div class="wpb_wrapper">
                        <div class="row wpb_row inner row-fluid cta-section row-has-fill">
                            <div class="wpb_column column_container col-sm-12 col-md-9">
                                <div class="column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="section-head mobile-center text-light">
                                            <h6><span>Tertarik penawaran kredit kami?</span></h6>
                                            <h2 class="section-title">Coba simulasi kredit sekarang!</h2>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column column_container col-sm-12 col-md-3">
                                <div class="column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="empty_space_12 sm-hidden"></div>
                                        <div class="text-right mobile-center light-hover">
                                            <a class="btn" href="{{ route('simulasi') }}">COBA SEKARANG</a>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- <aside>
    <section class="wpb_row row-fluid section-padd bg-light row-has-fill">
        <div class="container">
            <div class="row">
                <div class="col-12" style="margin-bottom: 50px">
                    <h2 class="custom-heading text-center">Simulasi Pinjaman Anda</h2>
                </div>
                <div class="col-12 text-center">
                    <div class="col-12">
                        Jumlah Pinjaman
                        <span id="resultTotalPinjaman" style="margin-left:91px; font-weight: bold">:</span>
                    </div>
                    <div class="col-12">
                        Lama Pinjaman
                        <span id="resultLamaPinjaman" style="margin-left:101px; font-weight: bold">:</span>
                    </div>
                    <div class="col-12">
                        Bunga per Tahun
                        <span id="resultBungaPertahun" style=" margin-left:93px; font-weight: bold">:</span>
                    </div>
                    <div class="col-12">
                        Angsuran Pokok Per Bulan
                        <span id="resultAngPokokBulan" style="margin-left:30px; font-weight: bold">:</span>
                    </div>
                    <div class="col-12">
                        Angsuran Bunga Per Bulan
                        <span id="resultAngBungaBulan" style="margin-left:29px; font-weight: bold">:</span>
                    </div>
                    <div class="col-12">
                        Total Angsuran Per Bulan
                        <span id="resultAngBulan" style="margin-left:38px; font-weight: bold">:</span>
                    </div>
                </div>
                <div class="wpb_column column_container col-sm-12" style="margin-top: 30px">
                    <table id="tableAngsuran" style="margin-left:auto;margin-right:auto">
                        <thead>
                            <tr>
                                <th>Bulan Ke-</th>
                                <th>Pokok</th>
                                <th>Bunga</th>
                                <th>Angsuran</th>
                                <th>Sisa Pinjaman</th>
                            </tr>
                        </thead>
                        <tbody id="table_list">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</aside> --}}