<!DOCTYPE html>
<html>

<head>
    <!-- Head V-->
    @include('component.head')
    @include('component.head-additional')
</head>

<body class="royal_preloader">
    <div id="page" class="site">
        <!-- Header V-->
        @include('component.header')

        <div id="content" class="site-content">
            <!-- hero V-->
            @include('component.hero')

            <!-- Fitur bawah hero V-->
            @include('component.feature-u-hero')

            <!-- Tentang Kami V-->
            @include('component.about-us')

            <!-- service V-->
            @include('component.services-2')

            <!-- service 2 V-->
            {{-- @include('component.services') --}}

            <!-- Simulasi V-->
            @include('component.simulasi')

            <!-- Berita atau Blog V-->
            {{-- @include('component.blog') --}}
        </div>

        <!-- footer V-->
        @include('component.footer')
    </div>

    <!-- Script V -->
    @include('component.script')
    @include('component.script-additional')
</body>

</html>