<section class="wpb_row row-fluid section-padd">
    <div class="container">
        <div class="row">
            <div class="wpb_column column_container col-sm-12 col-md-9">
                <div class="column-inner">
                    <div class="wpb_wrapper">
                        <div class="section-head">
                            <h6><span>PRODUK</span></h6>
                            <h2 class="section-title">Produk Unggulan Kami</h2>
                        </div>

                        <div class="empty_space_30 md-hidden sm-hidden"></div>
                    </div>
                </div>
            </div>
            <div class="wpb_column column_container col-sm-12 col-md-3">
                <div class="column-inner">
                    <div class="wpb_wrapper">
                        <div class="wpb_text_column wpb_content_element text-right mobile-left">
                            <div class="wpb_wrapper">
                                <p><a class="pagelink gray" href="services.html">All services</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- //////// -->
            <div class="wpb_column column_container col-sm-6 col-md-6">
                <div class="column-inner">
                    <div class="wpb_wrapper">
                        <div class="service-box image-box">
                            <img src="images/2.jpg" alt="Kredit KBPR Wedarijaksa" />
                            <div class="content-box">
                                <h3>Kredit</h3>
                                <p style="text-align: justify; margin-top: -10px">
                                    KBPR Wedarijaksa menyediakan fasilitas kredit untuk : Modal kerja,
                                    Investasi pendukung usaha misanya untuk membeli mesin, Konsumsi
                                    misalnya untuk biaya pendidikan anak.
                                </p>
                                <a class="link-box pagelink" href="service-detail.html">Baca selengkapnya</a>
                            </div>
                        </div>

                        <div class="empty_space_30"></div>
                    </div>
                </div>
            </div>
            <div class="wpb_column column_container col-sm-6 col-md-6">
                <div class="column-inner">
                    <div class="wpb_wrapper">
                        <div class="service-box image-box">
                            <img src="images/1.jpg" alt="Deposito KBPR Wedarijaksa" />
                            <div class="content-box">
                                <h3>Deposito Berjangka</h3>
                                <p style="text-align: justify; margin-top: -10px">
                                    Simpanan masyarakat yang ditempatkan di Bank, dimana penarikannya
                                    hanya dapat dilakukan pada waktu bertentu, berdasarkan perjanjian
                                    nasabah penyimpan dengan bank (time deposit).
                                </p>
                                <a class="link-box pagelink" href="service-detail.html">Baca selengkapnya</a>
                            </div>
                        </div>

                        <div class="empty_space_30"></div>
                    </div>
                </div>
            </div>
            <div class="wpb_column column_container col-sm-6 col-md-6">
                <div class="column-inner">
                    <div class="wpb_wrapper">
                        <div class="service-box image-box">
                            <img src="images/1.jpg" alt="Tamasya KBPR Wedarijaksa" />
                            <div class="content-box">
                                <h3>Tabungan Masyarakat (Tamasya)</h3>
                                <p style="text-align: justify; margin-top: -10px">
                                    Produk Bank yang ditujukan untuk masyarakat umum, dimana uang yang
                                    disetorkan tidak berjangka waktu dan tidak ada tanggal jatuh
                                    temponya. Nasabah bisa mengambil uangnya sewaktu-waktu jika nasabah
                                    membutuhkan, sesuai prosedur bank.
                                </p>
                                <a class="link-box pagelink" href="service-detail.html">Baca selengkapnya</a>
                            </div>
                        </div>

                        <div class="empty_space_30"></div>
                    </div>
                </div>
            </div>
            <div class="wpb_column column_container col-sm-6 col-md-6">
                <div class="column-inner">
                    <div class="wpb_wrapper">
                        <div class="service-box image-box">
                            <img src="images/1.jpg" alt="Simpel KBPR Wedarijaksa" />
                            <div class="content-box">
                                <h3>Simpanan Pelajar (Simpel)</h3>
                                <p style="text-align: justify; margin-top: -10px">
                                    Tabungan untuk para Siswa atas nama sendiri tanpa biaya administrasi
                                    bulanan, bebas biaya ganti buku dan tutup rekening, dan juga bunga
                                    menarik.
                                </p>
                                <a class="link-box pagelink" href="service-detail.html">Baca selengkapnya</a>
                            </div>
                        </div>

                        <div class="empty_space_30"></div>
                    </div>
                </div>
            </div>
            <!-- ////// -->
        </div>
    </div>
</section>