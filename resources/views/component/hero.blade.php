{{-- <section id="section-slider" class="fullwidthbanner-container custom-margin" aria-label="section-slider">
    <div id="revolution-slider" class="rev_slider fullscreenbanner">
        <ul>
            <li data-transition="fade" data-slotamount="10" data-masterspeed="default" data-thumb="">
                <!--  BACKGROUND IMAGE -->
                <img src="/images/banner/1.jpg" alt="" data-bgposition="center center" data-bgfit="cover"
                    data-bgrepeat="no-repeat" data-bgparallax="10" />
            </li>

            <li data-transition="fade" data-slotamount="10" data-masterspeed="default" data-thumb="">
                <!--  BACKGROUND IMAGE -->
                <img src="/images/banner/1.jpg" alt="" data-bgposition="center center" data-bgfit="cover"
                    data-bgrepeat="no-repeat" data-bgparallax="10" />
            </li>

            <li data-transition="fade" data-slotamount="10" data-masterspeed="default" data-thumb="">
                <!--  BACKGROUND IMAGE -->
                <img src="/images/banner/1.jpg" alt="" data-bgposition="center center" data-bgfit="cover"
                    data-bgrepeat="no-repeat" data-bgparallax="10" />
            </li>
        </ul>
    </div>
</section> --}}

<section class="wpb_row row-fluid row-full-width row-no-padding custom-margin">
    <div class="row">
        <div class="wpb_column column_container col-sm-12">
            <div class="column-inner">
                <div class="wpb_wrapper">
                    <div class="image-carousel" data-arrow="true">

                        <div>
                            <div class="image-item">
                                {{-- <img src="/images/banner/2.jpg" alt=""> --}}
                                <img src="/images/banner/1-melayani.jpg"
                                    alt="KBPR Wedarijaksa Melayani Kredit Deposito Tabungan"
                                    style="object-fit: cover;height: 100%;width: 100%;">
                            </div>
                        </div>

                        <div>
                            <div class=" image-item">
                                <img src="/images/banner/2-jam.jpg"
                                    alt="KBPR Wedarijaksa Melayani Senin-Jumat pukul 08.00-16.00 WIB"
                                    style="object-fit: cover;height: 100%;width: 100%;">
                            </div>
                        </div>

                        <div>
                            <div class=" image-item">
                                <img src="/images/banner/3-kredit.jpg"
                                    alt="KBPR Wedarijaksa Melayani Kredit Multiguna Bunga Hanya 2% per Bulan"
                                    style="object-fit: cover;height: 100%;width: 100%;">
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>