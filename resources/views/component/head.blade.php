<meta charset="utf-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta name="viewport" content="width=device-width, initial-scale=1" />
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">
{{-- <title>Koperasi BPR Wedarijaksa</title> --}}
{!! SEO::generate() !!}
{{-- {!! SEOMeta::generate() !!}
{!! OpenGraph::generate() !!}
{!! Twitter::generate() !!}
{!! JsonLd::generate() !!} --}}

<link rel="stylesheet" id="bootstrap-css" href="{{ asset('css/bootstrap.css') }}" type="text/css" media="all" />
<link rel="stylesheet" id="awesome-font-css" href="{{ asset('css/font-awesome.css') }}" type="text/css" media="all" />
<link rel="stylesheet" id="ionicon-font-css" href="{{ asset('css/ionicon.css') }}" type="text/css" media="all" />
<link rel="stylesheet" id="royal-preload-css" href="{{ asset('css/royal-preload.css') }}" type="text/css" media="all" />
<link rel="stylesheet" id="slick-slider-css" href="{{ asset('css/slick.css') }}" type="text/css" media="all" />
<link rel="stylesheet" id="slick-theme-css" href="{{ asset('css/slick-theme.css') }}" type="text/css" media="all" />
<link rel="stylesheet" id="consultax-style-css" href="{{ asset('style.css') }}" type="text/css" media="all" />

<link rel="shortcut icon" href="{{ asset('/images/favicon.png') }}" />