<section class="wpb_row row-fluid section-padd bg-light">
    <div class="container">
        <div class="row">
            <div class="wpb_column column_container col-sm-12 col-md-12">
                <div class="column-inner">
                    <div class="wpb_wrapper">
                        <div class="row wpb_row inner row-fluid">
                            <div class="wpb_column column_container col-sm-12 col-md-8">
                                <div class="column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="section-head">
                                            <h6><span>BERITA KAMI</span></h6>
                                            <h2 class="section-title">Kabar Terbaru Kami</h2>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column column_container col-sm-12 col-md-4">
                                <div class="column-inner">
                                    <div class="wpb_wrapper">
                                        <div class="
                                                wpb_text_column wpb_content_element
                                                text-right
                                                mobile-left
                                            ">
                                            <div class="wpb_wrapper">
                                                <p>
                                                    <a class="pagelink gray" href="{{ route('berita') }}">Tampilkan
                                                        Semua</a>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="empty_space_30 md-hidden sm-hidden"></div>
                    </div>
                </div>
            </div>
            <!-- Blog -->
            <div class="wpb_column column_container col-sm-12 col-md-12">
                <div class="column-inner">
                    <div class="wpb_wrapper">
                        <div class="news-slider posts-grid row" data-show="3" data-auto="true">

                            @foreach ($berita as $item)
                            <div>
                                <article class="news-item content-area">
                                    <div class="inner-item radius-top">
                                        <div class="thumb-image">
                                            <a href="{{ $item['url'] }}">
                                                <img src="{{ $item['image1'] }}" alt="{{ $item['judul'] }}"
                                                    style="height: 230px; object-fit:cover; object-position: center;" />
                                            </a>
                                        </div>
                                        <div class="inner-post radius-bottom">
                                            <div class="entry-meta">
                                                <span class="posted-on">
                                                    <time class="entry-date">{{ $item['tanggal'] }}</time>
                                                </span>
                                            </div>
                                            <h4 class="entry-title">
                                                <a href="{{ $item['url'] }}">{{ $item['judul'] }}</a>
                                            </h4>
                                            <p>
                                                {{ \Str::limit($item['body'], 150, $end='...') }}
                                            </p>
                                            <a class="post-link" href="{{ $item['url'] }}">Baca selengkapnya</a>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            @endforeach


                            {{-- <div>
                                <article class="news-item content-area">
                                    <div class="inner-item radius-top">
                                        <div class="thumb-image">
                                            <a href="post.html">
                                                <img src="/images/blog/2.jpg" alt="" />
                                            </a>
                                        </div>
                                        <div class="inner-post radius-bottom">
                                            <div class="entry-meta">
                                                <span class="posted-on">
                                                    <time class="entry-date">Rabu, 29 Desember 2021, 16:32 WIB</time>
                                                </span>
                                            </div>
                                            <h4 class="entry-title">
                                                <a href="post.html">Perbarindo Dampingi 5.000 Pelaku UMKM Jateng agar
                                                    Melek Digital</a>
                                            </h4>
                                            <p>
                                                Dalam upaya mendorong kebangkitan UMKM di Jawa Tengah, Perhimpunan Bank
                                                Perkreditan Rakyat Indonesia (Perbarindo) memberikan pendampingan
                                                digitalisasi untuk 5.000 pelaku UMKM di provinsi setempat...
                                            </p>
                                            <a class="post-link" href="post.html">Baca selengkapnya</a>
                                        </div>
                                    </div>
                                </article>
                            </div>

                            <div>
                                <article class="news-item content-area">
                                    <div class="inner-item radius-top">
                                        <div class="thumb-image">
                                            <a href="post.html">
                                                <img src="/images/blog/3.jpg" alt="" />
                                            </a>
                                        </div>
                                        <div class="inner-post radius-bottom">
                                            <div class="entry-meta">
                                                <span class="posted-on">
                                                    <time class="entry-date">Selasa, 21 Desember 2021, 14:43 WIB</time>
                                                </span>
                                            </div>
                                            <h4 class="entry-title">
                                                <a href="post.html">Perbarindo Galang Bantuan Insan BPR untuk Korban
                                                    Semeru</a>
                                            </h4>
                                            <p>
                                                Perhimpunam Bank Perkreditan Rakyat Indonesia (Perbarind) menggalang
                                                bantuan dari Bank Perkreditan Rakyat (BPR) dan Bank Perkreditan Rakyat
                                                Syariah (BPRS) untuk korban erupsi Gunung Semeru...
                                            </p>
                                            <a class="post-link" href="post.html">Baca selengkapnya</a>
                                        </div>
                                    </div>
                                </article>
                            </div> --}}
                        </div>
                        <div class="empty_space_45 lg-hidden"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>