<section class="wpb_row row-fluid-top bg-light">
    <div class="container">
        <div class="row">
            <div class="wpb_column column_container col-sm-12 col-md-12">
                <div class="column-inner">
                    <div class="wpb_wrapper">
                        <div class="section-head ">
                            <h6><span>Produk</span></h6>
                            <h2 class="section-title">Produk Unggulan Kami</h2>
                        </div>

                        <div class="empty_space_30 md-hidden sm-hidden"><span class="empty_space_inner"></span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="wpb_row row-fluid section-padd-bot row-has-fill row-full-width row-no-padding bg-light">
    <div class="row">
        <div class="wpb_column column_container col-sm-12">
            <div class="column-inner">
                <div class="wpb_wrapper">
                    <div class="project-list-2">
                        <div class="project-slider-2 projects" data-show="1" data-auto="" data-dot="true">

                            <div class="col-md-12">
                                <div class="project-item">
                                    <div class="slide-img"><img src="images/servis/kredit.jpg"
                                            alt="Kredit KBPR Wedarijaksa"></div>
                                    <div class="inner row">
                                        <div class="col-md-12">
                                            <h4><a href="{{ route('kredit') }}">Kredit</a></h4>
                                            <p>
                                                KBPR Wedarijaksa menyediakan fasilitas kredit untuk : Modal kerja,
                                                Investasi pendukung usaha misanya untuk membeli mesin, Konsumsi misalnya
                                                untuk biaya pendidikan anak.
                                            </p>
                                            <a class="pagelink gray" href="{{ route('kredit') }}">Lihat detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="project-item">
                                    <div class="slide-img"><img src="images/servis/deposito.jpg"
                                            alt="Deposito KBPR Wedarijaksa"></div>
                                    <div class="inner row">
                                        <div class="col-md-12">
                                            <h4><a href="{{ route('deposito') }}">Deposito Berjangka</a></h4>
                                            <p>
                                                Simpanan masyarakat yang ditempatkan di Bank, dimana penarikannya hanya
                                                dapat dilakukan pada waktu bertentu, berdasarkan perjanjian nasabah
                                                penyimpan dengan bank (time deposit).
                                            </p>
                                            <a class="pagelink gray" href="{{ route('deposito') }}">Lihat detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="project-item">
                                    <div class="slide-img"><img src="images/servis/tamasya.jpg"
                                            alt="Tamasya KBPR Wedarijaksa"></div>
                                    <div class="inner row">
                                        <div class="col-md-12">
                                            <h4><a href="{{ route('tamasya') }}">Tabungan Masyarakat (Tamasya)</a></h4>
                                            <p>
                                                Produk Bank yang ditujukan untuk masyarakat umum, dimana uang yang
                                                disetorkan tidak berjangka waktu dan tidak ada tanggal jatuh temponya.
                                                Nasabah bisa mengambil uangnya sewaktu-waktu jika nasabah membutuhkan,
                                                sesuai prosedur bank.
                                            </p>
                                            <a class="pagelink gray" href="{{ route('tamasya') }}">Lihat detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="project-item">
                                    <div class="slide-img"><img src="images/servis/simpel.jpg"
                                            alt="Simpel KBPR Wedarijaksa"></div>
                                    <div class="inner row">
                                        <div class="col-md-12">
                                            <h4><a href="{{ route('simpel') }}">Simpanan Pelajar (Simpel)</a></h4>
                                            <p>
                                                Tabungan untuk para Siswa atas nama sendiri tanpa biaya administrasi
                                                bulanan, bebas biaya ganti buku dan tutup rekening, dan juga bunga
                                                menarik.
                                            </p>
                                            <a class="pagelink gray" href="{{ route('simpel') }}">Lihat detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="project-item">
                                    <div class="slide-img"><img src="images/servis/kurban.jpg"
                                            alt="Tabungan Kurban KBPR Wedarijaksa"></div>
                                    <div class="inner row">
                                        <div class="col-md-12">
                                            <h4><a href="{{ route('kurban') }}">Tabungan Kurban (Coming Soon)</a></h4>
                                            <p>
                                                Tabungan untuk persiapan pembelian hewan kurban pada Hari Raya Idul
                                                Adha. Coming Soon segera!
                                            </p>
                                            <a class="pagelink gray" href="{{ route('kurban') }}">Lihat detail</a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="container">
                            <div class="arrows-slick">
                                <button type="button" class="btn-left slick-arrow prev-nav"><i
                                        class="fa fa-angle-left"></i></button>
                                <button type="button" class="btn-right slick-arrow next-nav"><i
                                        class="fa fa-angle-right"></i></button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>