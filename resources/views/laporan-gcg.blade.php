<!DOCTYPE html>
<html>

<head>
    <!-- Head V-->
    @include('component.head')
</head>

<body class="royal_preloader">
    <div id="page" class="site">
        <!-- Header V-->
        @include('component.header')

        <div id="content" class="site-content custom-margin">
            {{-- Gambar diatas --}}
            <div class="page-header">
                <div class="container">
                    <div class="breadc-box no-line">
                        <div class="row">
                            <div class="col-md-6">
                                <h1 class="page-title">Laporan GCG</h1>
                            </div>
                            <div class="col-md-6 mobile-left text-right">
                                <ul id="breadcrumbs" class="breadcrumbs none-style">
                                    <li><a href="{{ route('home') }}">Home</a></li>
                                    <li><a href="{{ route('gcg') }}">Laporan</a></li>
                                    <li class="active">Laporan GCG</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Main Content --}}
            <div class="entry-content">
                <div class="container">
                    <div class="row">

                        <div id="primary" class="content-area col-lg-8 col-md-8 col-sm-12 col-xs-12 pull-right">
                            <main id="main" class="site-main">

                                <article class="ot_service type-ot_service status-publish has-post-thumbnail hentry">
                                    <div class="inner-post">
                                        <section class="wpb_row row-fluid">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="wpb_column column_container col-sm-12">
                                                        <div class="column-inner">
                                                            <div class="wpb_wrapper">
                                                                <h2>Laporan GCG</h2>
                                                                <div class="wpb_text_column wpb_content_element">
                                                                    <div class="wpb_wrapper">
                                                                        {{-- <h4>Keuntungan Deposito Berjangka</h4> --}}
                                                                        <ul>
                                                                            <li>
                                                                                Laporan GCG Tahun 2021 -
                                                                                <a href="{{ asset('laporan/2021_laporan_gcg.pdf') }}" 
                                                                                    download="Laporan Transparansi Tata Kelola Tahun 2021 Asosiasi.pdf">
                                                                                    Download File PDF 
                                                                                </a>
                                                                                atau
                                                                                <a href="{{ asset('laporan/2021_laporan_gcg.pdf') }}"
                                                                                    target="_blank">
                                                                                    Baca Selengkapnya
                                                                                </a>
                                                                            </li>
                                                                            <li>
                                                                                Laporan GCG Tahun 2023 -
                                                                                <a href="{{ asset('laporan/2023_laporan_gcg.pdf') }}" 
                                                                                    download="Laporan Transparansi Tata Kelola Tahun 2023 Asosiasi.pdf">
                                                                                    Download File PDF 
                                                                                </a>
                                                                                atau
                                                                                <a href="{{ asset('laporan/2023_laporan_gcg.pdf') }}"
                                                                                    target="_blank">
                                                                                    Baca Selengkapnya
                                                                                </a>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </article>
                            </main>
                        </div>

                        {{-- Sidebar --}}
                        <aside id="sidebar" class="widget-area service-sidebar col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <section id="nav_menu-1" class="widget widget_nav_menu">
                                <h4 class="widget-title">Laporan</h4>
                                <div class="menu-service-menu-container">
                                    <ul id="menu-service-menu" class="menu">
                                        {{-- <li><a href="{{ route('tahunan') }}">Laporan Tahunan</a></li> --}}
                                        <li><a href="{{ route('triwulan') }}">Laporan Triwulan</a></li>
                                        <li class="current-menu-item">
                                            <a href="{{ route('gcg') }}">Laporan GCG</a>
                                        </li>
                                    </ul>
                                </div>
                            </section>
                        </aside>

                    </div>
                </div>
            </div>
        </div>

        <!-- footer V-->
        @include('component.footer')
    </div>

    <!-- Script V -->
    @include('component.script')
</body>

</html>