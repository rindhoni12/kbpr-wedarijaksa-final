<!DOCTYPE html>
<html>

<head>
    <!-- Head V-->
    @include('component.head')
</head>

<body class="royal_preloader">
    <div id="page" class="site">
        <!-- Header V-->
        @include('component.header')

        <div id="content" class="site-content custom-margin">
            {{-- Gambar diatas --}}
            <div class="page-header">
                <div class="container">
                    <div class="breadc-box no-line">
                        <div class="row">
                            <div class="col-md-6">
                                <h1 class="page-title">Tentang Kami</h1>
                            </div>
                            <div class="col-md-6 mobile-left text-right">
                                <ul id="breadcrumbs" class="breadcrumbs none-style">
                                    <li><a href="{{ route('home') }}">Home</a></li>
                                    <li class="active">Tentang Kami</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Main Content --}}
            <div class="entry-content">
                <div class="container">
                    <div class="boxed-content">

                        {{-- Tentang Kami --}}
                        <section class="wpb_row row-fluid section-padd">
                            <div class="container">
                                <div class="row">
                                    <div class="wpb_column column_container col-sm-12">
                                        <div class="column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="section-head ">
                                                    <h6><span>TENTANG KAMI</span></h6>
                                                    <h2 class="section-title">Koperasi BPR Wedarijaksa</h2>
                                                </div>

                                                <div class="wpb_text_column wpb_content_element">
                                                    <div class="wpb_wrapper">
                                                        <p>
                                                            Koperasi BPR Wedarijaksa Kabupaten Pati (KBPR Wedarijaksa)
                                                            berlokasi di Jalan Raya Juana-Tayu Km.7, Guyangan Kecamatan
                                                            Trangkil Kabupaten Pati. Didirikan berdasarkan Surat
                                                            Pengurus Koperasi BPR Wedarijaksa No: 21/KBPR/IV/1990
                                                            tanggal 2 April 1990 dan No : 24/KBPR/VI/1990 tanggal 18
                                                            Juni 1990 perihal permohonan izin usaha Koperasi BPR
                                                            Wedarijaksa di Kecamatan Wedarijaksa, Kabupaten Pati, Jawa
                                                            Tengah dan dengan surat Menteri Keuangan No.
                                                            S-944/MK.13/1989 tanggal 9 Agustus 1989
                                                            {{-- <a class="pagelink" href="#direksi">Our team</a> --}}
                                                        </p>

                                                    </div>
                                                </div>

                                                <div class="ot-socials ">
                                                    <span>Follow us:</span>
                                                    <a target="_blank" href="#"><i class="fa fa-facebook"></i></a>
                                                    <a target="_blank" href="#"><i class="fa fa-twitter"></i></a>
                                                    <a target="_blank" href="#"><i class="fa fa-pinterest-p"></i></a>
                                                    <a target="_blank" href="#"><i class="fa fa-linkedin"></i></a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        {{-- Image SLider --}}
                        <section class="wpb_row row-fluid row-full-width row-no-padding">
                            <div class="row">
                                <div class="wpb_column column_container col-sm-12">
                                    <div class="column-inner">
                                        <div class="wpb_wrapper">
                                            <div class="image-carousel" data-show="1" data-arrow="true">

                                                <div>
                                                    <div class="image-item">
                                                        <img src="/images/1.jpg" alt="Foto Depan KBPR Wedarijaksa">
                                                    </div>
                                                </div>

                                                <div>
                                                    <div class="image-item">
                                                        <img src="/images/2.jpg" alt="Foto Samping KBPR Wedarijaksa">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        {{-- Visi dan Misi --}}
                        <section class="wpb_row row-fluid section-padd">
                            <div class="container">
                                <div class="row">
                                    <div class="wpb_column column_container col-sm-12">
                                        <div class="column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="section-head ">
                                                    <h6><span>Tentang Kami</span></h6>
                                                    <h2 class="section-title">Visi dan Misi</h2>
                                                </div>

                                                {{-- <div class="wpb_text_column wpb_content_element ">
                                                    <div class="wpb_wrapper">
                                                        <p>When, while the lovely valley teems with vapour around me,
                                                            and the meridian sun strikes the upper surface of the
                                                            impenetrable foliage of my trees, and but a few stray gleams
                                                            steal into the inner sanctuary, I throw myself down among
                                                            the tall grass by the trickling stream; and, as I lie close
                                                            to the earth by the trickling stream. <a class="pagelink"
                                                                href="#">Our services</a></p>

                                                    </div>
                                                </div> --}}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column column_container col-sm-12">
                                        <div class="column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="row wpb_row inner row-fluid row-o-equal-height row-flex">
                                                    <div
                                                        class="radius-left custom-padd-2 wpb_column column_container col-sm-12 col-md-6 col-has-fill">
                                                        <div class="column-inner">
                                                            <div class="wpb_wrapper"></div>
                                                        </div>
                                                    </div>
                                                    <div
                                                        class="bg-second radius-right wpb_column column_container col-sm-12 col-md-6">
                                                        <div class="column-inner">
                                                            <div class="wpb_wrapper">
                                                                <div
                                                                    class="wpb_text_column wpb_content_element text-light padding-box">
                                                                    <div class="wpb_wrapper">
                                                                        <h2 class="section-title">VISI</h2>
                                                                        <p class="lead">
                                                                            <em>
                                                                                “Menjadi BPR yang kuat, dipercaya dan
                                                                                selalu dihati masyarakat di seluruh
                                                                                wilayah kerja Kabupaten Pati dan
                                                                                sekitarnya.”
                                                                            </em>
                                                                        </p>
                                                                        <h2 class="section-title">MISI</h2>
                                                                        <p class="lead">
                                                                            <em>
                                                                                “Menjadi BPR yang kuat, dipercaya dan
                                                                                selalu dihati masyarakat dengan
                                                                                berkomitmen untuk pengembangan UMKM
                                                                                dengan pelayanan terbaik, dapat
                                                                                dipercaya, dan menjadi tumpuan
                                                                                masyarakat disekitar Wilayah Kabupaten
                                                                                Pati dan sekitarnya.”
                                                                            </em>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="wpb_column column_container col-sm-12">
                                        <div class="column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="section-head has-line">
                                                    <div class="empty_space_45"></div>
                                                    <h6><span>Terdaftar dan Diawasi oleh</span></h6>
                                                </div>
                                                <div class="empty_space_60"></div>
                                                <div class="partner-slider image-carousel text-center" data-show="4"
                                                    data-arrow="false">

                                                    <div>
                                                        <div class="partner-item text-center clearfix">
                                                            <div class="inner">
                                                                <div class="thumb">
                                                                    <img src="/images/diawasi/ojk.png" alt="OJK"
                                                                        style="margin-top: 15px; width:125px" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div>
                                                        <div class="partner-item text-center clearfix">
                                                            <div class="inner">
                                                                <div class="thumb">
                                                                    <img src="/images/diawasi/bpr.png" alt="BPR"
                                                                        style="width:50px" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div>
                                                        <div class="partner-item text-center clearfix">
                                                            <div class="inner">
                                                                <div class="thumb">
                                                                    <img src="/images/diawasi/ayobank.png"
                                                                        alt="AYOKEBANK"
                                                                        style="margin-top: 15px; width:155px" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div>
                                                        <div class="partner-item text-center clearfix">
                                                            <div class="inner">
                                                                <div class="thumb">
                                                                    <img src="/images/diawasi/lps.png" alt="LPS"
                                                                        style="margin-top: 15px; width:125px" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        {{-- Struktur Organisasi --}}
                        <section class="wpb_row row-fluid no-bot">
                            <div class="container">
                                <div class="row">
                                    <div class="wpb_column column_container col-sm-12">
                                        <div class="column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="section-head ">
                                                    <h6><span>Tentang Kami</span></h6>
                                                    <h2 class="section-title">Struktur Organisasi</h2>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column column_container col-sm-12 col-md-12">
                                        <div class="column-inner">
                                            <div class="wpb_wrapper">
                                                <img src="/images/struktur_organisasi.jpeg"
                                                    alt="Struktur Organisasi KBPR Wedarijaksa">
                                                <div class="empty_space_30 lg-hidden"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                        {{-- Lokasi Kantor --}}
                        <section class="wpb_row row-fluid section-padd no-bot">
                            <div class="container">
                                <div class="row">
                                    <div class="wpb_column column_container col-sm-12 col-md-12">
                                        <div class="column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="section-head ">
                                                    <h6><span>Lokasi Kantor</span></h6>
                                                    <h2 class="section-title">
                                                        Jl. Raya Juana-Tayu Km.7, Guyangan Kec. Trangkil Kab. Pati
                                                    </h2>
                                                </div>

                                                <div class="empty_space_12"><span class="empty_space_inner"></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column column_container col-sm-12 col-md-8">
                                        <div class="column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="wpb_text_column wpb_content_element contact-info">
                                                    <div class="wpb_wrapper">
                                                        <p>
                                                            <a href="mailto:kbpr.pati@gmail.com">
                                                                <i class="fa fa-envelope">
                                                                    kbpr.pati@gmail.com
                                                                </i>
                                                            </a>
                                                            <a href="https://api.whatsapp.com/send?phone=6281392006441"
                                                                target="_blank">
                                                                <i class="fa fa-whatsapp">
                                                                    +6281392006441
                                                                </i>
                                                            </a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="wpb_column column_container col-sm-12 col-md-4">
                                        <div class="column-inner">
                                            <div class="wpb_wrapper">
                                                <div
                                                    class="wpb_text_column wpb_content_element socials text-right md-hidden sm-hidden">
                                                    <div class="wpb_wrapper">
                                                        <p>
                                                            <a href="#" target="_blank" rel="noopener noreferrer">
                                                                <i class="fa fa-facebook-official">fb</i>
                                                            </a>
                                                            <a href="#" target="_blank" rel="noopener noreferrer">
                                                                <i class="fa fa-twitter">tw</i>
                                                            </a>
                                                            <a href="#" target="_blank" rel="noopener noreferrer">
                                                                <i class="fa fa-pinterest">pr</i>
                                                            </a>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section style="margin-top: 30px">
                            <div class="container">
                                <div class="row">
                                    <iframe
                                        src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3962.8803638379372!2d111.09416161436485!3d-6.66174566697648!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e772ad5771a3afb%3A0xe749d504c33e504!2sKoperasi%20Bank%20Perkreditan%20Rakyat!5e0!3m2!1sid!2sid!4v1641217024980!5m2!1sid!2sid"
                                        width="1170" height="500" style="border:0;" allowfullscreen="" loading="lazy">
                                    </iframe>
                                </div>
                            </div>
                        </section>

                        <section class="wpb_row row-fluid section-padd no-bot">
                            <div class="container">
                                <div class="row">
                                    <div class="wpb_column column_container col-sm-12">
                                        <div class="column-inner">
                                            <div class="wpb_wrapper">
                                                <div class="gray-line">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>

                    </div>
                </div>
            </div>
        </div>

        <!-- footer V-->
        @include('component.footer')
    </div>

    <!-- Script V -->
    @include('component.script')
</body>

</html>