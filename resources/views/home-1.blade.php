<!DOCTYPE html>
<html>

<head>
    @include('components.head')
</head>

<body class="royal_preloader">
    <div id="page" class="site">
        <header id="site-header" class="site-header mobile-header-blue header-style-1">
            @include('components.navbar-top')
            <div class="main-header md-hidden sm-hidden">
                @include('components.navbar-middle')
                @include('components.navbar-bottom')
            </div>
            @include('components.navbar-mobile')
        </header>

        <div id="content" class="site-content">
            @include('components.hero-slider')
            @include('components.section-services-u-hero')
            @include('components.section-services')
            @include('components.section-callback')
            @include('components.section-blog')
        </div>
        @include('components.footer')
    </div>

    @include('components.script')
</body>

</html>