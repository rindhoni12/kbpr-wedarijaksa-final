<!DOCTYPE html>
<html>

<head>
    <!-- Head V-->
    @include('component.head')
</head>

<body class="royal_preloader">
    <div id="page" class="site">
        <!-- Header V-->
        @include('component.header')

        <div id="content" class="site-content custom-margin">
            {{-- Gambar diatas --}}
            <div class="page-header">
                <div class="container">
                    <div class="breadc-box no-line">
                        <div class="row">
                            <div class="col-md-6">
                                <h1 class="page-title">Tabungan Masyarakat</h1>
                            </div>
                            <div class="col-md-6 mobile-left text-right">
                                <ul id="breadcrumbs" class="breadcrumbs none-style">
                                    <li><a href="{{ route('home') }}">Home</a></li>
                                    <li><a href="{{ route('kredit') }}">Layanan Kami</a></li>
                                    <li class="active">Tabungan Masyarakat</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {{-- Main Content --}}
            <div class="entry-content">
                <div class="container">
                    <div class="row">

                        <div id="primary" class="content-area col-lg-8 col-md-8 col-sm-12 col-xs-12 pull-right">
                            <main id="main" class="site-main">

                                <article class="ot_service type-ot_service status-publish has-post-thumbnail hentry">
                                    <div class="inner-post">
                                        <section class="wpb_row row-fluid">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="wpb_column column_container col-sm-12">
                                                        <div class="column-inner">
                                                            <div class="wpb_wrapper">
                                                                <h2>Tabungan Masyarakat (Tamasya)</h2>
                                                                <img src="/images/servis/tamasya.jpg"
                                                                    alt="Tamasya KBPR Wedarijaksa"
                                                                    style="margin-bottom: 20px">
                                                                <div class="wpb_text_column wpb_content_element">
                                                                    <div class="wpb_wrapper">
                                                                        <p>
                                                                            <strong>Tabungan Masyarakat
                                                                                (Tamasya)</strong> sebuah produk
                                                                            bank yang ditujukan untuk masyarakat umum,
                                                                            dimana uang yang disetorkan tidak berjangka
                                                                            waktu dan tidak ada tanggal jatuh temponya.
                                                                            Nasabah bisa mengambil uangnya sewaktu-waktu
                                                                            jika nasabah membutuhkan, sesuai prosedur
                                                                            bank.
                                                                        </p>
                                                                        <h4>Syarat Pembukaan Rekening</h4>
                                                                        <ol>
                                                                            <li>
                                                                                Fotocopy KTP yang masih berlaku sebanyak
                                                                                1
                                                                                (satu) lembar
                                                                            </li>
                                                                            <li>
                                                                                Fotocopy KTP ahli waris yang ditunjuk
                                                                                sebanyak 1 (satu) lembar
                                                                            </li>
                                                                            <li>
                                                                                Mengisi formulir identitas calon nasabah
                                                                                dengan lengkap dan jelas
                                                                            </li>
                                                                            <li>
                                                                                Mengisi slip penyetoran Tabungan
                                                                                Masyarakat dengan setoran pertama
                                                                                sekurang-kurangnya sebesar Rp 50.000,-
                                                                                (lima
                                                                                puluh ribu rupiah)
                                                                            </li>
                                                                            <li>
                                                                                Setoran selanjutnya sekurang-kurangnya
                                                                                sebesar Rp 10.000,- (sepuluh ribu
                                                                                rupiah)
                                                                            </li>
                                                                        </ol>
                                                                        <h4>Ketentuan Tabungan Masyarakat</h4>
                                                                        <ul>
                                                                            <li>
                                                                                Tabungan Masyarakat untuk semua lapisan
                                                                                masyarakat
                                                                            </li>
                                                                            <li>
                                                                                Tidak ada potongan biaya administrasi
                                                                                setiap bulannya
                                                                            </li>
                                                                            <li>
                                                                                Bunga Tabungan Masyarakat secara
                                                                                otomatis
                                                                                masuk ke rekening nasabah (Automatic
                                                                                Roll
                                                                                Over)
                                                                            </li>
                                                                            <li>
                                                                                Sebagai bukti tabungan, Bank akan
                                                                                menerbitkan Buku Tabungan atas nama
                                                                                penabung
                                                                            </li>
                                                                            <li>
                                                                                Bisa diambil sewaktu-waktu jika nasabah
                                                                                membutuhkan pada hari kerja
                                                                            </li>
                                                                        </ul>
                                                                        <h4>Suku Bunga Tabungan Masyarakat</h4>
                                                                        <p>
                                                                            Suku bunga kompetitif
                                                                            Menguntungkan
                                                                            Besar suku bunga adalah 4,8%
                                                                            per tahun atau 0,4% setiap bulan
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                </article>
                            </main>
                        </div>

                        {{-- Sidebar --}}
                        <aside id="sidebar" class="widget-area service-sidebar col-lg-4 col-md-4 col-sm-12 col-xs-12">
                            <section id="nav_menu-1" class="widget widget_nav_menu">
                                <h4 class="widget-title">Layanan Kami</h4>
                                <div class="menu-service-menu-container">
                                    <ul id="menu-service-menu" class="menu">
                                        <li><a href="{{ route('kredit') }}">Kredit</a></li>
                                        <li><a href="{{ route('deposito') }}">Deposito</a></li>
                                        <li class="current-menu-item">
                                            <a href="{{ route('tamasya') }}">Tabungan Masyarakat (Tamasya)</a>
                                        </li>
                                        <li><a href="{{ route('simpel') }}">Simpanan Pelajar (Simpel)</a></li>
                                        <li><a href="{{ route('kurban') }}">Tabungan Kurban (Coming Soon)</a></li>
                                    </ul>
                                </div>
                            </section>
                            @include('component.layanan-sidebar')
                        </aside>

                    </div>
                </div>
            </div>
        </div>

        <!-- footer V-->
        @include('component.footer')
    </div>

    <!-- Script V -->
    @include('component.script')
</body>

</html>