<?php

use App\Http\Controllers\KreditController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('simulasi-kredit');
// });

Route::get('/', [KreditController::class, 'index'])->name('home');

Route::get('/layanan-kami/kredit', [KreditController::class, 'kredit'])->name('kredit');
Route::get('/layanan-kami/deposito', [KreditController::class, 'deposito'])->name('deposito');
Route::get('/layanan-kami/tabungan-masyarakat', [KreditController::class, 'tamasya'])->name('tamasya');
Route::get('/layanan-kami/simpanan-pelajar', [KreditController::class, 'simpel'])->name('simpel');
Route::get('/layanan-kami/tabungan-kurban', [KreditController::class, 'kurban'])->name('kurban');

Route::get('/laporan/laporan-tahunan', [KreditController::class, 'laporanTahunan'])->name('tahunan');
Route::get('/laporan/laporan-triwulan', [KreditController::class, 'laporanTriwulan'])->name('triwulan');
Route::get('/laporan/laporan-gcg', [KreditController::class, 'laporanGcg'])->name('gcg');

Route::get('/tentang-kami', [KreditController::class, 'tentang'])->name('tentang');

Route::get('/simulasi-kredit', [KreditController::class, 'simulasiKredit'])->name('simulasi');
Route::post('/hitung-kredit', [KreditController::class, 'hitungKredit'])->name('hitung.kredit');
Route::get('/simulasi-kredit-bunga-putus', [KreditController::class, 'simulasiKreditBungaPutus'])->name('simulasi.putus');
Route::post('/hitung-kredit-bunga-putus', [KreditController::class, 'hitungKreditWithBungaPutus'])->name('hitung.kredit.putus');

Route::get('/berita-kami', [KreditController::class, 'berita'])->name('berita');
Route::get('/berita-kami/pelatihan-apu-ppt-kbpr-wedarijaksa-pati', [KreditController::class, 'berita1']);
Route::get('/berita-kami/sosialisasi-gerakan-indonesia-menabung-bersama-kbpr-wedarijaksa', [KreditController::class, 'berita2']);
Route::get('/berita-kami/kbpr-wedarijaksa-melaksanakan-edukasi-perbankan-produk-simpanan-pelajar', [KreditController::class, 'berita3']);
Route::get('/berita-kami/laga-persahabatan-antara-kbpr-wedarijaksa-dengan-ksp-sms-kudus', [KreditController::class, 'berita4']);
Route::get('/berita-kami/kbpr-wedarijaksa-peduli-lingkungan-membagiakan-bantuan-air-bersih-untuk-daerah-kekeringan-di-kabupaten-pati', [KreditController::class, 'berita5']);
Route::get('/berita-kami/seluruh-karyawan-kbpr-wedarijaksa-outbound-akhir-tahun-di-ungaran', [KreditController::class, 'berita6']);
Route::get('/berita-kami/pendidikan-dan-latihan-service-excellent-untuk-seluruh-karyawan-kbpr-wedarijaksa', [KreditController::class, 'berita7']);

Route::get('/sitemap', [KreditController::class, 'sitemap']); 
Route::get('/sitemap/posts', [KreditController::class, 'sitemapPosts']); 
Route::get('/cpanel', [KreditController::class, 'cpanel']); 