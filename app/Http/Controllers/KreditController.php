<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Artesaos\SEOTools\Facades\SEOMeta;
use Artesaos\SEOTools\Facades\OpenGraph;
use Artesaos\SEOTools\Facades\TwitterCard;
use Artesaos\SEOTools\Facades\JsonLd;
use Artesaos\SEOTools\Facades\JsonLdMulti;
use Artesaos\SEOTools\Facades\SEOTools;
use Illuminate\Support\Facades\Storage;
use Spatie\Sitemap\Sitemap;
use Spatie\Sitemap\Tags\Url;

class KreditController extends Controller
{
    public function index()
    {
        SEOMeta::setTitle('Koperasi BPR Wedarijaksa');
        SEOMeta::setDescription('Koperasi BPR Wedarijaksa adalah sebuah BPR yang berdiri sejak 1990, Melayani Kredit Deposito dan Tabungan');
        SEOMeta::setCanonical('https://kbprwedarijaksa.com');

        OpenGraph::setDescription('Koperasi BPR Wedarijaksa adalah sebuah BPR yang berdiri sejak 1990, Melayani Kredit Deposito dan Tabungan');
        OpenGraph::setTitle('Home - Koperasi BPR Wedarijaksa');
        OpenGraph::setUrl('https://kbprwedarijaksa.com');
        OpenGraph::addProperty('type', 'articles');
        OpenGraph::addImage('https://kbprwedarijaksa.com/images/1.jpg');

        TwitterCard::setTitle('Home - Koperasi BPR Wedarijaksa');
        TwitterCard::setSite('@kbprwedarijaksa');

        JsonLd::setTitle('Home - Koperasi BPR Wedarijaksa');
        JsonLd::setDescription('Koperasi BPR Wedarijaksa adalah sebuah BPR yang berdiri sejak 1990, Melayani Kredit Deposito dan Tabungan');
        JsonLd::addImage('https://kbprwedarijaksa.com/images/logo-kbpr3.png');

        $berita = $this->konten_utama();

        return view('component.master', compact('berita'));
    }

    public function kredit()
    {
        SEOMeta::setTitle('Kredit');
        SEOMeta::setDescription('KBPR Wedarijaksa menyediakan fasilitas kredit kepada nasabah dengan cepat, mudah dan memuaskan untuk keperluan');
        SEOMeta::setCanonical('https://kbprwedarijaksa.com/layanan-kami/kredit');

        OpenGraph::setDescription('KBPR Wedarijaksa menyediakan fasilitas kredit kepada nasabah dengan cepat, mudah dan memuaskan untuk keperluan');
        OpenGraph::setTitle('Kredit - Koperasi BPR Wedarijaksa');
        OpenGraph::setUrl('https://kbprwedarijaksa.com/layanan-kami/kredit');
        OpenGraph::addProperty('type', 'articles');
        OpenGraph::addImage('https://kbprwedarijaksa.com/images/servis/kredit.jpg');

        TwitterCard::setTitle('Kredit - Koperasi BPR Wedarijaksa');
        TwitterCard::setSite('@kbprwedarijaksa');

        JsonLd::setTitle('Kredit - Koperasi BPR Wedarijaksa');
        JsonLd::setDescription('KBPR Wedarijaksa menyediakan fasilitas kredit kepada nasabah dengan cepat, mudah dan memuaskan untuk keperluan');
        JsonLd::addImage('https://kbprwedarijaksa.com/images/servis/kredit.jpg');

        return view('layanan-kredit');
    }

    public function deposito()
    {
        SEOMeta::setTitle('Deposito');
        SEOMeta::setDescription('KBPR Wedarijaksa menyediakan Deposito Berjangka adalah simpanan masyarakat yang ditempatkan dibank, dimana penarikannya hanya dapat dilakukan pada waktu bertentu, berdasarkan perjanjian nasabah penyimpan dengan bank (time deposit)');
        SEOMeta::setCanonical('https://kbprwedarijaksa.com/layanan-kami/deposito');

        OpenGraph::setDescription('KBPR Wedarijaksa menyediakan Deposito Berjangka adalah simpanan masyarakat yang ditempatkan dibank, dimana penarikannya hanya dapat dilakukan pada waktu bertentu, berdasarkan perjanjian nasabah penyimpan dengan bank (time deposit)');
        OpenGraph::setTitle('Deposito - Koperasi BPR Wedarijaksa');
        OpenGraph::setUrl('https://kbprwedarijaksa.com/layanan-kami/deposito');
        OpenGraph::addProperty('type', 'articles');
        OpenGraph::addImage('https://kbprwedarijaksa.com/images/servis/deposito.jpg');

        TwitterCard::setTitle('Deposito - Koperasi BPR Wedarijaksa');
        TwitterCard::setSite('@kbprwedarijaksa');

        JsonLd::setTitle('Deposito - Koperasi BPR Wedarijaksa');
        JsonLd::setDescription('KBPR Wedarijaksa menyediakan Deposito Berjangka adalah simpanan masyarakat yang ditempatkan dibank, dimana penarikannya hanya dapat dilakukan pada waktu bertentu, berdasarkan perjanjian nasabah penyimpan dengan bank (time deposit)');
        JsonLd::addImage('https://kbprwedarijaksa.com/images/servis/deposito.jpg');

        return view('layanan-deposito');
    }

    public function tamasya()
    {
        SEOMeta::setTitle('Tamasya');
        SEOMeta::setDescription('KBPR Wedarijaksa menyediakan Tabungan Masyarakat (Tamasya) sebuah produk bank yang ditujukan untuk masyarakat umum, dimana uang yang disetorkan tidak berjangka waktu dan tidak ada tanggal jatuh temponya. Nasabah bisa mengambil uangnya sewaktu-waktu jika nasabah membutuhkan, sesuai prosedur bank');
        SEOMeta::setCanonical('https://kbprwedarijaksa.com/layanan-kami/tabungan-masyarakat');

        OpenGraph::setDescription('KBPR Wedarijaksa menyediakan Tabungan Masyarakat (Tamasya) sebuah produk bank yang ditujukan untuk masyarakat umum, dimana uang yang disetorkan tidak berjangka waktu dan tidak ada tanggal jatuh temponya. Nasabah bisa mengambil uangnya sewaktu-waktu jika nasabah membutuhkan, sesuai prosedur bank');
        OpenGraph::setTitle('Tamasya - Koperasi BPR Wedarijaksa');
        OpenGraph::setUrl('https://kbprwedarijaksa.com/layanan-kami/tabungan-masyarakat');
        OpenGraph::addProperty('type', 'articles');
        OpenGraph::addImage('https://kbprwedarijaksa.com/images/servis/tamasya.jpg');

        TwitterCard::setTitle('Tamasya - Koperasi BPR Wedarijaksa');
        TwitterCard::setSite('@kbprwedarijaksa');

        JsonLd::setTitle('Tamasya - Koperasi BPR Wedarijaksa');
        JsonLd::setDescription('KBPR Wedarijaksa menyediakan Tabungan Masyarakat (Tamasya) sebuah produk bank yang ditujukan untuk masyarakat umum, dimana uang yang disetorkan tidak berjangka waktu dan tidak ada tanggal jatuh temponya. Nasabah bisa mengambil uangnya sewaktu-waktu jika nasabah membutuhkan, sesuai prosedur bank');
        JsonLd::addImage('https://kbprwedarijaksa.com/images/servis/tamasya.jpg');

        return view('layanan-tamasya');
    }

    public function simpel()
    {
        SEOMeta::setTitle('Simpanan Pelajar');
        SEOMeta::setDescription('KBPR Wedarijaksa menyediakan Simpanan Pelajar (Simpel) merupakan tabungan untuk siswa yang diterbitkan secara nasional oleh bank-bank di Indonesia, dengan persyaratan mudah dan sederhana, dilengkapi dengan fitur yang menarik, guna mendorong budaya menabung sejak dini');
        SEOMeta::setCanonical('https://kbprwedarijaksa.com/layanan-kami/simpanan-pelajar');

        OpenGraph::setDescription('KBPR Wedarijaksa menyediakan Simpanan Pelajar (Simpel) merupakan tabungan untuk siswa yang diterbitkan secara nasional oleh bank-bank di Indonesia, dengan persyaratan mudah dan sederhana, dilengkapi dengan fitur yang menarik, guna mendorong budaya menabung sejak dini');
        OpenGraph::setTitle('Simpanan Pelajar - Koperasi BPR Wedarijaksa');
        OpenGraph::setUrl('https://kbprwedarijaksa.com/layanan-kami/simpanan-pelajar');
        OpenGraph::addProperty('type', 'articles');
        OpenGraph::addImage('https://kbprwedarijaksa.com/images/servis/simpel.jpg');

        TwitterCard::setTitle('Simpanan Pelajar - Koperasi BPR Wedarijaksa');
        TwitterCard::setSite('@kbprwedarijaksa');

        JsonLd::setTitle('Simpanan Pelajar - Koperasi BPR Wedarijaksa');
        JsonLd::setDescription('KBPR Wedarijaksa menyediakan Simpanan Pelajar (Simpel) merupakan tabungan untuk siswa yang diterbitkan secara nasional oleh bank-bank di Indonesia, dengan persyaratan mudah dan sederhana, dilengkapi dengan fitur yang menarik, guna mendorong budaya menabung sejak dini');
        JsonLd::addImage('https://kbprwedarijaksa.com/images/servis/simpel.jpg');

        return view('layanan-simpel');
    }

    public function kurban()
    {
        SEOMeta::setTitle('Tabungan Kurban');
        SEOMeta::setDescription('KBPR Wedarijaksa menyediakan Tabungan Kurban Tabungan untuk persiapan pembelian hewan kurban pada Hari Raya Idul Adha. Kami sedang menyiapkan layanan kami yang satu ini supaya bisa segera diluncurkan ke masyarakat');
        SEOMeta::setCanonical('https://kbprwedarijaksa.com/layanan-kami/tabungan-kurban');

        OpenGraph::setDescription('KBPR Wedarijaksa menyediakan Tabungan Kurban Tabungan untuk persiapan pembelian hewan kurban pada Hari Raya Idul Adha. Kami sedang menyiapkan layanan kami yang satu ini supaya bisa segera diluncurkan ke masyarakat');
        OpenGraph::setTitle('Tabungan Kurban - Koperasi BPR Wedarijaksa');
        OpenGraph::setUrl('https://kbprwedarijaksa.com/layanan-kami/tabungan-kurban');
        OpenGraph::addProperty('type', 'articles');
        OpenGraph::addImage('https://kbprwedarijaksa.com/images/servis/kurban.jpg');

        TwitterCard::setTitle('Tabungan Kurban - Koperasi BPR Wedarijaksa');
        TwitterCard::setSite('@kbprwedarijaksa');

        JsonLd::setTitle('Tabungan Kurban - Koperasi BPR Wedarijaksa');
        JsonLd::setDescription('KBPR Wedarijaksa menyediakan Tabungan Kurban Tabungan untuk persiapan pembelian hewan kurban pada Hari Raya Idul Adha. Kami sedang menyiapkan layanan kami yang satu ini supaya bisa segera diluncurkan ke masyarakat');
        JsonLd::addImage('https://kbprwedarijaksa.com/images/servis/kurban.jpg');

        return view('layanan-kurban');
    }

    public function laporanGcg()
    {
        SEOMeta::setTitle('Laporan GCG');
        SEOMeta::setDescription('Laporan GCG Koperasi BPR Wedarijaksa');
        SEOMeta::setCanonical('https://kbprwedarijaksa.com/laporan/laporan-gcg');

        OpenGraph::setDescription('Laporan GCG Koperasi BPR Wedarijaksa');
        OpenGraph::setTitle('Laporan GCG - Koperasi BPR Wedarijaksa');
        OpenGraph::setUrl('https://kbprwedarijaksa.com/laporan/laporan-gcg');
        OpenGraph::addProperty('type', 'articles');
        OpenGraph::addImage('https://kbprwedarijaksa.com/images/1.jpg');

        TwitterCard::setTitle('Laporan GCG - Koperasi BPR Wedarijaksa');
        TwitterCard::setSite('@kbprwedarijaksa');

        JsonLd::setTitle('Laporan GCG - Koperasi BPR Wedarijaksa');
        JsonLd::setDescription('Laporan GCG Koperasi BPR Wedarijaksa');
        JsonLd::addImage('https://kbprwedarijaksa.com/images/logo-kbpr3.png');

        return view('laporan-gcg');
    }

    public function laporanTahunan()
    {
        SEOMeta::setTitle('Laporan Tahunan');
        SEOMeta::setDescription('Laporan Tahunan Koperasi BPR Wedarijaksa');
        SEOMeta::setCanonical('https://kbprwedarijaksa.com/laporan/laporan-tahunan');

        OpenGraph::setDescription('Laporan Tahunan Koperasi BPR Wedarijaksa');
        OpenGraph::setTitle('Laporan Tahunan - Koperasi BPR Wedarijaksa');
        OpenGraph::setUrl('https://kbprwedarijaksa.com/laporan/laporan-tahunan');
        OpenGraph::addProperty('type', 'articles');
        OpenGraph::addImage('https://kbprwedarijaksa.com/images/1.jpg');

        TwitterCard::setTitle('Laporan Tahunan - Koperasi BPR Wedarijaksa');
        TwitterCard::setSite('@kbprwedarijaksa');

        JsonLd::setTitle('Laporan Tahunan - Koperasi BPR Wedarijaksa');
        JsonLd::setDescription('Laporan Tahunan Koperasi BPR Wedarijaksa');
        JsonLd::addImage('https://kbprwedarijaksa.com/images/logo-kbpr3.png');

        return view('laporan-tahunan');
    }

    public function laporanTriwulan()
    {
        SEOMeta::setTitle('Laporan Triwulan');
        SEOMeta::setDescription('Laporan Triwulan Koperasi BPR Wedarijaksa');
        SEOMeta::setCanonical('https://kbprwedarijaksa.com/laporan/laporan-triwulan');

        OpenGraph::setDescription('Laporan Triwulan Koperasi BPR Wedarijaksa');
        OpenGraph::setTitle('Laporan Triwulan - Koperasi BPR Wedarijaksa');
        OpenGraph::setUrl('https://kbprwedarijaksa.com/laporan/laporan-triwulan');
        OpenGraph::addProperty('type', 'articles');
        OpenGraph::addImage('https://kbprwedarijaksa.com/images/1.jpg');

        TwitterCard::setTitle('Laporan Triwulan - Koperasi BPR Wedarijaksa');
        TwitterCard::setSite('@kbprwedarijaksa');

        JsonLd::setTitle('Laporan Triwulan - Koperasi BPR Wedarijaksa');
        JsonLd::setDescription('Laporan Triwulan Koperasi BPR Wedarijaksa');
        JsonLd::addImage('https://kbprwedarijaksa.com/images/logo-kbpr3.png');

        return view('laporan-triwulan');
    }

    public function tentang()
    {
        SEOMeta::setTitle('Tentang Kami');
        SEOMeta::setDescription('Tentang Kami Koperasi BPR Wedarijaksa Kabupaten Pati (KBPR Wedarijaksa) berlokasi di Jalan Raya Juana-Tayu Km.7, Guyangan Kecamatan Trangkil Kabupaten Pati');
        SEOMeta::setCanonical('https://kbprwedarijaksa.com/tentang-kami');

        OpenGraph::setDescription('Tentang Kami Koperasi BPR Wedarijaksa Kabupaten Pati (KBPR Wedarijaksa) berlokasi di Jalan Raya Juana-Tayu Km.7, Guyangan Kecamatan Trangkil Kabupaten Pati');
        OpenGraph::setTitle('Tentang Kami - Koperasi BPR Wedarijaksa');
        OpenGraph::setUrl('https://kbprwedarijaksa.com/tentang-kami');
        OpenGraph::addProperty('type', 'articles');
        OpenGraph::addImage('https://kbprwedarijaksa.com/images/1.jpg');

        TwitterCard::setTitle('Tentang Kami - Koperasi BPR Wedarijaksa');
        TwitterCard::setSite('@kbprwedarijaksa');

        JsonLd::setTitle('Tentang Kami - Koperasi BPR Wedarijaksa');
        JsonLd::setDescription('Tentang Kami Koperasi BPR Wedarijaksa Kabupaten Pati (KBPR Wedarijaksa) berlokasi di Jalan Raya Juana-Tayu Km.7, Guyangan Kecamatan Trangkil Kabupaten Pati');
        JsonLd::addImage('https://kbprwedarijaksa.com/images/1.jpg');

        return view('tentang-kami');
    }

    public function simulasiKredit()
    {
        SEOMeta::setTitle('Simulasi Kredit Standar');
        SEOMeta::setDescription('Simulasi Kredit Standar dari KBPR Wedarijaksa Simulasi ini untuk memudahkan calon kreditur mengetahui besaran angsuran per-bulan yang harus dibayarkan dan besarannya sudah sesuai aturan bunga yang ditetapkan perusahaan');
        SEOMeta::setCanonical('https://kbprwedarijaksa.com/simulasi-kredit');

        OpenGraph::setDescription('Simulasi Kredit Standar dari KBPR Wedarijaksa Simulasi ini untuk memudahkan calon kreditur mengetahui besaran angsuran per-bulan yang harus dibayarkan dan besarannya sudah sesuai aturan bunga yang ditetapkan perusahaan');
        OpenGraph::setTitle('Simulasi Kredit Standar - Koperasi BPR Wedarijaksa');
        OpenGraph::setUrl('https://kbprwedarijaksa.com/simulasi-kredit');
        OpenGraph::addProperty('type', 'articles');
        OpenGraph::addImage('https://kbprwedarijaksa.com/images/1.jpg');

        TwitterCard::setTitle('Simulasi Kredit Standar - Koperasi BPR Wedarijaksa');
        TwitterCard::setSite('@kbprwedarijaksa');

        JsonLd::setTitle('Simulasi Kredit Standar - Koperasi BPR Wedarijaksa');
        JsonLd::setDescription('Simulasi Kredit Standar dari KBPR Wedarijaksa Simulasi ini untuk memudahkan calon kreditur mengetahui besaran angsuran per-bulan yang harus dibayarkan dan besarannya sudah sesuai aturan bunga yang ditetapkan perusahaan');
        JsonLd::addImage('https://kbprwedarijaksa.com/images/logo-kbpr3.png');

        return view('simulasi-kredit-2');
    }
    
    public function simulasiKreditBungaPutus()
    {
        SEOMeta::setTitle('Simulasi Kredit Bunga Putus');
        SEOMeta::setDescription('Simulasi Kredit Bunga Putus dari KBPR Wedarijaksa Simulasi ini untuk memudahkan calon kreditur mengetahui besaran angsuran per-bulan yang harus dibayarkan dan besarannya sudah sesuai aturan bunga yang ditetapkan perusahaan Kredit Bunga Putus memiliki kelebihan dimana kredit bisa dilunasi lebih cepat dari jangka waktu yang ditetapkan diawal perjanjian');
        SEOMeta::setCanonical('https://kbprwedarijaksa.com/simulasi-kredit-bunga-putus');

        OpenGraph::setDescription('Simulasi Kredit Bunga Putus dari KBPR Wedarijaksa Simulasi ini untuk memudahkan calon kreditur mengetahui besaran angsuran per-bulan yang harus dibayarkan dan besarannya sudah sesuai aturan bunga yang ditetapkan perusahaan Kredit Bunga Putus memiliki kelebihan dimana kredit bisa dilunasi lebih cepat dari jangka waktu yang ditetapkan diawal perjanjian');
        OpenGraph::setTitle('Simulasi Kredit Bunga Putus - Koperasi BPR Wedarijaksa');
        OpenGraph::setUrl('https://kbprwedarijaksa.com/simulasi-kredit-bunga-putus');
        OpenGraph::addProperty('type', 'articles');
        OpenGraph::addImage('https://kbprwedarijaksa.com/images/1.jpg');

        TwitterCard::setTitle('Simulasi Kredit Bunga Putus - Koperasi BPR Wedarijaksa');
        TwitterCard::setSite('@kbprwedarijaksa');

        JsonLd::setTitle('Simulasi Kredit Bunga Putus - Koperasi BPR Wedarijaksa');
        JsonLd::setDescription('Simulasi Kredit Bunga Putus dari KBPR Wedarijaksa Simulasi ini untuk memudahkan calon kreditur mengetahui besaran angsuran per-bulan yang harus dibayarkan dan besarannya sudah sesuai aturan bunga yang ditetapkan perusahaan Kredit Bunga Putus memiliki kelebihan dimana kredit bisa dilunasi lebih cepat dari jangka waktu yang ditetapkan diawal perjanjian');
        JsonLd::addImage('https://kbprwedarijaksa.com/images/logo-kbpr3.png');

        return view('simulasi-kredit-bunga-putus');
    }

    public function hitungKredit(Request $request)
    {
        // dd($request->all());

        $jumlahKredit = $request->jumlahKredit;
        $jangkaWaktu  = $request->jangkaWaktu;

        if ($jangkaWaktu == 10){
            $sukubungaPertahun = 30;
        }
        else if ($jangkaWaktu == 20){
            $sukubungaPertahun = 24;
        }

        $angsuran = [];
        $sukuBunga = $sukubungaPertahun / 100; //utk persen bunga per tahun
        $pokok = $jumlahKredit / $jangkaWaktu; //utk cari nilai pokok
        $bunga = $jumlahKredit * $sukuBunga / 12; //cari bunga per bulannya
        $sisaPinjaman = $jumlahKredit;
        $jumlahAngsuran = $pokok + $bunga; //angsurannya

        for($i = 0; $i < $jangkaWaktu; $i++) {
            $sisaPinjaman -= $pokok;  //cek sisa pinjaman
            array_push($angsuran, [
                "no"                => $i + 1,
                "pokok"             => round($pokok),
                "bunga"             => round($bunga),
                "jumlahAngsuran"    => round($jumlahAngsuran),
                "sisaPinjaman"      => round($sisaPinjaman)
            ]);
        }
        // return $angsuran;
        
        // $json_response = json_encode($angsuran);
        // echo $json_response;

        $data['angsuran'] = $angsuran;
        $data['jumlahKredit'] = $jumlahKredit;
        $data['jangkaWaktu'] = $jangkaWaktu;
        $data['sukubungaPertahun'] = $sukubungaPertahun/12;
        $data['pokok'] = $pokok;
        $data['bunga'] = $bunga;
        $data['jumlahAngsuran'] = $jumlahAngsuran;
        

        return response()->json($data);
    }

    public function hitungKreditWithBungaPutus(Request $request)
    {
        // dd($request->all());

        $jumlahKredit = $request->jumlahKredit;
        $jangkaWaktu  = $request->jangkaWaktu;
        $bungaPutus   = $request->bungaPutus;

        if ($jangkaWaktu == 10){
            $sukubungaPertahun = 30;
        }
        else if ($jangkaWaktu == 20){
            $sukubungaPertahun = 24;
        }

        $angsuran = [];
        $sukuBunga = $sukubungaPertahun / 100; //utk persen bunga per tahun
        $pokok = $jumlahKredit / $jangkaWaktu; //utk cari nilai pokok
        $bunga = $jumlahKredit * $sukuBunga / 12; //cari bunga per bulannya
        $sisaPinjaman = $jumlahKredit;
        $jumlahAngsuran = $pokok + $bunga; //angsurannya

        for($i = 0; $i < $bungaPutus-1; $i++) {
            $sisaPinjaman -= $pokok;  //cek sisa pinjaman 3500 
            array_push($angsuran, [
                "no"                => $i + 1,
                "pokok"             => round($pokok),
                "bunga"             => round($bunga),
                "jumlahAngsuran"    => round($jumlahAngsuran),
                "sisaPinjaman"      => round($sisaPinjaman)
            ]);
        }

        $angsuranTerakhir = $sisaPinjaman + $bunga;
        array_push($angsuran, [
            "no"                => $bungaPutus,
            "pokok"             => round($sisaPinjaman),
            "bunga"             => round($bunga),
            "jumlahAngsuran"    => round($angsuranTerakhir),
            "sisaPinjaman"      => 0
        ]);
        // return $angsuran;
        
        // $json_response = json_encode($angsuran);
        // echo $json_response;

        $data['angsuran'] = $angsuran;
        $data['jumlahKredit'] = $jumlahKredit;
        $data['jangkaWaktu'] = $jangkaWaktu;
        $data['sukubungaPertahun'] = $sukubungaPertahun/12;
        $data['pokok'] = $pokok;
        $data['bunga'] = $bunga;
        $data['jumlahAngsuran'] = $jumlahAngsuran;
        $data['bungaPutus'] = $bungaPutus;
        

        return response()->json($data);
    }

    public function berita()
    {
        SEOMeta::setTitle('Berita Kami');
        SEOMeta::setDescription('Berita Terbaru dari KBPR Wedarijaksa');
        SEOMeta::setCanonical('https://kbprwedarijaksa.com/berita-kami');

        OpenGraph::setDescription('Berita Terbaru dari KBPR Wedarijaksa');
        OpenGraph::setTitle('Berita Kami - Koperasi BPR Wedarijaksa');
        OpenGraph::setUrl('https://kbprwedarijaksa.com/berita-kami');
        OpenGraph::addProperty('type', 'articles');
        OpenGraph::addImage('https://kbprwedarijaksa.com/images/berita/1-apu-ppt.jpg');

        TwitterCard::setTitle('Berita Kami - Koperasi BPR Wedarijaksa');
        TwitterCard::setSite('@kbprwedarijaksa');

        JsonLd::setTitle('Berita Kami - Koperasi BPR Wedarijaksa');
        JsonLd::setDescription('Berita Terbaru dari KBPR Wedarijaksa');
        JsonLd::addImage('https://kbprwedarijaksa.com/images/berita/1-apu-ppt.jpg');

        $json = Storage::disk('local')->get('berita.json');
        $berita = json_decode($json, true);

        $json = Storage::disk('local')->get('berita-side.json');
        $berita_side = json_decode($json, true);

        return view('berita', compact('berita', 'berita_side'));
    }

    // Berita Start
    private function konten_utama()
    {
        $json = Storage::disk('local')->get('berita.json');
        $berita = json_decode($json, true);
        return $berita;
    }

    private function konten_side()
    {
        $json = Storage::disk('local')->get('berita-side.json');
        $berita_side = json_decode($json, true);
        return $berita_side;
    }

    public function berita1()
    {
        SEOTools::setTitle('Pelatihan APU PPT KBPR Wedarijaksa Pati');
        SEOTools::setDescription('Penerapan program APU DAN PPT merupakan kewajiban bagi semua pihak, terutama bagi BANK sebagai Penyedia Jasa Keuangan (PJK). Dengan perkembangan teknologi komunikasi & informasi yang sangat pesat,  mendorong bank untuk lebih  konperhenship dalam memitigasi resiko, bank tidak manfaatkan oleh pelaku tindak pidana pencucian uang (TTPU) dan tindak pidana pendanaan terorisme(TPPT)');
        SEOTools::opengraph()->setUrl('https://kbprwedarijaksa.com/berita-kami/pelatihan-apu-ppt-kbpr-wedarijaksa-pati');
        SEOTools::setCanonical('https://kbprwedarijaksa.com/berita-kami/pelatihan-apu-ppt-kbpr-wedarijaksa-pati');
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::twitter()->setSite('@kbprwedarijaksa');
        SEOTools::jsonLd()->addImage('https://kbprwedarijaksa.com/images/berita/1-apu-ppt.jpg');

        $berita = $this->konten_utama()[0];
        $berita_side = $this->konten_side();

        return view('berita.berita1', compact('berita', 'berita_side'));
    }

    public function berita2()
    {
        SEOTools::setTitle('Sosialisasi Gerakan Indonesia Menabung Bersama KBPR Wedarijaksa');
        SEOTools::setDescription('Sebagai bentuk komitmen bank terhadap gerakan Indonesia menabung. KBPR Wedarijaksa secara rutin setiap tahun mengadakan sosialisasi tabungan untuk pelajar (SIMPEL), terutama mengahadapi tahun ajaran siswa baru dengan tujuan utamanya untuk menumbuhkan kebiasaan menabung sejak usia dini, sekaligus memberikan pengertian dan pemahaman kepada adik-adik pelajar tentang bank');
        SEOTools::opengraph()->setUrl('https://kbprwedarijaksa.com/berita-kami/sosialisasi-gerakan-indonesia-menabung-bersama-kbpr-wedarijaksa');
        SEOTools::setCanonical('https://kbprwedarijaksa.com/berita-kami/sosialisasi-gerakan-indonesia-menabung-bersama-kbpr-wedarijaksa');
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::twitter()->setSite('@kbprwedarijaksa');
        SEOTools::jsonLd()->addImage('https://kbprwedarijaksa.com/images/berita/2-ayo-menabung.jpg');

        $berita = $this->konten_utama()[1];
        $berita_side = $this->konten_side();

        return view('berita.berita2', compact('berita', 'berita_side'));
    }

    public function berita3()
    {
        SEOTools::setTitle('KBPR Wedarijaksa Melaksanakan Edukasi Perbankan Produk Simpanan Pelajar');
        SEOTools::setDescription('Tidak ketinggalan pula, edukasi juga dilakukan pada anak-anak TPQ, Madrasah , Diniyah (MADIN), TPA dan Taman Kanak-kanak /RA yang berada di sekitar kantor  KBPR Wedarijaksa. Sehingga adik-adik yang belum memiliki KTP pun bisa dilayani dengan produk khusus untuk anak-anak pelajar berupa (Simpel)');
        SEOTools::opengraph()->setUrl('https://kbprwedarijaksa.com/berita-kami/kbpr-wedarijaksa-melaksanakan-edukasi-perbankan-produk-simpanan-pelajar');
        SEOTools::setCanonical('https://kbprwedarijaksa.com/berita-kami/kbpr-wedarijaksa-melaksanakan-edukasi-perbankan-produk-simpanan-pelajar');
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::twitter()->setSite('@kbprwedarijaksa');
        SEOTools::jsonLd()->addImage('https://kbprwedarijaksa.com/images/berita/3-edukasi-simpel.jpg');

        $berita = $this->konten_utama()[2];
        $berita_side = $this->konten_side();

        return view('berita.berita3', compact('berita', 'berita_side'));
    }

    public function berita4()
    {
        SEOTools::setTitle('Laga Persahabatan antara KBPR Wedarijaksa dengan KSP SMS Kudus BERSAMA KITA SUKSES MULIA');
        SEOTools::setDescription('Salah satu bentuk upaya meningkatkan kualitas dan produk sivitas SDM KBPR Wedarijaksa adalah menjaga kesehatan dan kebugaran fisik karyawan.  Disamping itu olahraga juga mampu memupuk kekompakan, kesetia kawan dan persatuan serta kerjasama dalam sebuah teamwork. Cabang-cabang olahraga yang bisa di pilih dan disukai antara lain Futsal, bulutangkis,  tenis meja dan bola volley');
        SEOTools::opengraph()->setUrl('https://kbprwedarijaksa.com/berita-kami/laga-persahabatan-antara-kbpr-wedarijaksa-dengan-ksp-sms-kudus');
        SEOTools::setCanonical('https://kbprwedarijaksa.com/berita-kami/laga-persahabatan-antara-kbpr-wedarijaksa-dengan-ksp-sms-kudus');
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::twitter()->setSite('@kbprwedarijaksa');
        SEOTools::jsonLd()->addImage('https://kbprwedarijaksa.com/images/berita/4-laga-persahabatan.jpg');

        $berita = $this->konten_utama()[3];
        $berita_side = $this->konten_side();

        return view('berita.berita4', compact('berita', 'berita_side'));
    }

    public function berita5()
    {
        SEOTools::setTitle('KBPR Wedarijaksa Peduli Lingkungan Membagiakan Bantuan Air Bersih untuk Daerah Kekeringan di Kabupaten Pati');
        SEOTools::setDescription('Kondisi kekeringan ketika kemarau penjaga masih sering menimpa desa – desa di wilayah Kabupaten Pati bagian selatan. Wilayah –wilayah tersebut juga merupakan wilayah potensial KBPR WEDARIJAKSA. Untuk mengurangi beban akibar bencana kekeringan bersebut, KBPR WEDARIJAKSA ikut membantu menyalurkan air bersih sebagai bentuk kepedulian terhadap masyarakat dalam hal penyediaan air bersih untuk keperluan hidup sehari-hari');
        SEOTools::opengraph()->setUrl('https://kbprwedarijaksa.com/berita-kami/kbpr-wedarijaksa-peduli-lingkungan-membagiakan-bantuan-air-bersih-untuk-daerah-kekeringan-di-kabupaten-pati');
        SEOTools::setCanonical('https://kbprwedarijaksa.com/berita-kami/kbpr-wedarijaksa-peduli-lingkungan-membagiakan-bantuan-air-bersih-untuk-daerah-kekeringan-di-kabupaten-pati');
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::twitter()->setSite('@kbprwedarijaksa');
        SEOTools::jsonLd()->addImage('https://kbprwedarijaksa.com/images/berita/5-kbpr-peduli.jpg');

        $berita = $this->konten_utama()[4];
        $berita_side = $this->konten_side();

        return view('berita.berita5', compact('berita', 'berita_side'));
    }

    public function berita6()
    {
        SEOTools::setTitle('Seluruh Karyawan KBPR Wedarijaksa Outbound Akhir Tahun di Ungaran');
        SEOTools::setDescription('Seluruh karyawan KBPR Wedarijaksa baru saja melakukan Outbound Akhir Tahun yang dilaksanakan di Hotel Green Valley. Kegiatan ini dalam upaya yang dilakukan oleh manajemen untuk menjaga kekompakan dan kebersamaan antar karyawan supaya mampu menhadapi tahun berikutnya dengan semangat dan kekuatan penuh. Perlombaan kecil-kecilan pun diadakan untuk memeriahkan suasana dan rangkaian diakhir dengan Pendidikan dan Pelatihan SERVICE EXCELLENT untuk Seluruh Karyawan KBPR Wedarijaksa');
        SEOTools::opengraph()->setUrl('https://kbprwedarijaksa.com/berita-kami/seluruh-karyawan-kbpr-wedarijaksa-outbound-akhir-tahun-di-ungaran');
        SEOTools::setCanonical('https://kbprwedarijaksa.com/berita-kami/seluruh-karyawan-kbpr-wedarijaksa-outbound-akhir-tahun-di-ungaran');
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::twitter()->setSite('@kbprwedarijaksa');
        SEOTools::jsonLd()->addImage('https://kbprwedarijaksa.com/images/berita/6-kbpr-outbound.jpg');

        $berita = $this->konten_utama()[5];
        $berita_side = $this->konten_side();

        return view('berita.berita6', compact('berita', 'berita_side'));
    }

    public function berita7()
    {
        SEOTools::setTitle('Pendidikan dan Latihan SERVICE EXCELLENT untuk Seluruh Karyawan KBPR Wedarijaksa');
        SEOTools::setDescription('Akhir tahun 2021 seluruh karyawan KBPR Wedarijaksa melakukan Field Trip Outbound ke Ungaran di Hotel Green Valley lebih tepatnya. Selain melakukan outbound, seluruh karyawan juga mengikuti kegiatan Pendidikan dan Latihan SERVICE EXCELLENT, Pelatihan Service Excellence mempelajari bagaimana caranya untuk berkomunikasi dengan sopan, ramah, menghargai orang lain. Bagaimana caranya yakin, percaya diri dalam berkomunikasi, bagaimana memiliki penampilan yang rapi, murah senyum, dan hal-hal lain yang terkait dengan Service Excellence Skills');
        SEOTools::opengraph()->setUrl('https://kbprwedarijaksa.com/berita-kami/pendidikan-dan-latihan-service-excellent-untuk-seluruh-karyawan-kbpr-wedarijaksa');
        SEOTools::setCanonical('https://kbprwedarijaksa.com/berita-kami/pendidikan-dan-latihan-service-excellent-untuk-seluruh-karyawan-kbpr-wedarijaksa');
        SEOTools::opengraph()->addProperty('type', 'articles');
        SEOTools::twitter()->setSite('@kbprwedarijaksa');
        SEOTools::jsonLd()->addImage('https://kbprwedarijaksa.com/images/berita/7-service-excellent.jpg');

        $berita = $this->konten_utama()[6];
        $berita_side = $this->konten_side();

        return view('berita.berita7', compact('berita', 'berita_side'));
    }
    // Berita End

    public function sitemap()
    {
        $sitemap = Sitemap::create()
        ->add(Url::create('/layanan-kami/kredit'))
        ->add(Url::create('/layanan-kami/deposito'))
        ->add(Url::create('/layanan-kami/tabungan-masyarakat'))
        ->add(Url::create('/layanan-kami/simpanan-pelajar'))
        ->add(Url::create('/layanan-kami/tabungan-kurban'))
        ->add(Url::create('/simulasi-kredit'))
        ->add(Url::create('/simulasi-kredit-bunga-putus'))
        ->add(Url::create('/laporan/laporan-tahunan'))
        ->add(Url::create('/laporan/laporan-triwulan'))
        ->add(Url::create('/laporan/laporan-gcg'))
        ->add(Url::create('/tentang-kami'))
        ->add(Url::create('/berita-kami'));

        $sitemap->writeToFile(public_path('sitemap.xml'));
    }

    public function sitemapPosts()
    {
        $sitemap = Sitemap::create();

        $berita = $this->konten_utama();

        foreach ($berita as $berita) {
            $url = $berita['url'];
            $sitemap->add(Url::create("/{$url}"));
        }
        $sitemap->writeToFile(public_path('sitemap-posts.xml'));
    }

    public function cpanel()
    {
        return abort(404);
    }
}
